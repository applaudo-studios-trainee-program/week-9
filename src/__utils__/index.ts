import { BrowserRouter } from 'react-router-dom';
import { ReactElement } from 'react';
import { render } from '@testing-library/react';

import { commonAPIResponseDataMock } from '__mocks__/common';

const BASE_URL = process.env.REACT_APP_BASE_URL;
const API_CONFIG = process.env.REACT_APP_API_CONFIG;

export const renderWithRouter = (ui: ReactElement, { route = '/' } = {}) => {
  window.history.pushState({}, 'Test Page', route);

  return render(ui, { wrapper: BrowserRouter });
};

export const getAPIResponse = (
  results: Record<string, unknown> | Record<string, unknown>[]
) => ({
  ...commonAPIResponseDataMock,
  data: {
    offset: 0,
    limit: 20,
    total: 1493,
    count: 20,
    results
  }
});

export const getBaseUrl = (endpoint: string, params?: string) =>
  params
    ? `${BASE_URL}${endpoint}?${API_CONFIG}&${params}`
    : `${BASE_URL}${endpoint}?${API_CONFIG}`;
