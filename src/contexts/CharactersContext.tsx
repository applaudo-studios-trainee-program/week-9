import { ReactNode, Dispatch, useReducer, createContext } from 'react';

import {
  CharactersActions,
  charactersReducer,
  CharactersState
} from 'reducers/charactersReducer';

type Props = { children: ReactNode };

const initialState: CharactersState = {
  data: [],
  offset: 0,
  total: 0,
  filterData: null,
  removedElements: []
};

export const CharactersContext = createContext<{
  charactersState: CharactersState;
  dispatchToCharacters: Dispatch<CharactersActions>;
} | null>(null);

export const CharactersContextProvider = ({ children }: Props) => {
  const [charactersState, dispatchToCharacters] = useReducer(
    charactersReducer,
    initialState
  );

  return (
    <CharactersContext.Provider
      value={{ charactersState, dispatchToCharacters }}
    >
      {children}
    </CharactersContext.Provider>
  );
};
