import { ReactNode, Dispatch, useReducer, createContext } from 'react';

import {
  ComicsActions,
  comicsReducer,
  ComicsState
} from 'reducers/comicsReducer';

type Props = { children: ReactNode };

const initialState: ComicsState = {
  data: [],
  offset: 0,
  total: 0,
  filterData: null,
  removedElements: []
};

export const ComicsContext = createContext<{
  comicsState: ComicsState;
  dispatchToComics: Dispatch<ComicsActions>;
} | null>(null);

export const ComicsContextProvider = ({ children }: Props) => {
  const [comicsState, dispatchToComics] = useReducer(
    comicsReducer,
    initialState
  );

  return (
    <ComicsContext.Provider value={{ comicsState, dispatchToComics }}>
      {children}
    </ComicsContext.Provider>
  );
};
