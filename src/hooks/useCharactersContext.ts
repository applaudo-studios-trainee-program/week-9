import { useContext } from 'react';

import { CharactersContext } from 'contexts/CharactersContext';

export const useCharactersContext = () => {
  const charactersContext = useContext(CharactersContext);

  if (charactersContext === null) {
    throw new Error(
      'useCharactersContext must be used within a CharactersProvider'
    );
  }

  return charactersContext;
};
