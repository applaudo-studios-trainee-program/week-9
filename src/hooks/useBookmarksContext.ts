import { useContext } from 'react';

import { BookmarksContext } from 'contexts/BookmarksContext';

export const useBookmarksContext = () => {
  const bookmarksContext = useContext(BookmarksContext);

  if (bookmarksContext === null) {
    throw new Error(
      'useBookmarksContext must be used within a BookmarksProvider'
    );
  }

  return bookmarksContext;
};
