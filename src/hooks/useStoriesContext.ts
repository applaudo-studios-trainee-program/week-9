import { useContext } from 'react';

import { StoriesContext } from 'contexts/StoriesContext';

export const useStoriesContext = () => {
  const storiesContext = useContext(StoriesContext);

  if (storiesContext === null) {
    throw new Error('useStoriesContext must be used within a StoriesProvider');
  }

  return storiesContext;
};
