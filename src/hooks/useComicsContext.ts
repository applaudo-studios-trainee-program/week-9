import { useContext } from 'react';

import { ComicsContext } from 'contexts/ComicsContext';

export const useComicsContext = () => {
  const comicsContext = useContext(ComicsContext);

  if (comicsContext === null) {
    throw new Error('useComicsContext must be used within a ComicsProvider');
  }

  return comicsContext;
};
