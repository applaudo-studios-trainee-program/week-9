import { ComicsActions, ComicsState } from 'reducers/comicsReducer';

export const setData = ({
  data,
  offset,
  total
}: Pick<ComicsState, 'data' | 'offset' | 'total'>): ComicsActions => ({
  type: 'SET_DATA',
  payload: { data, offset, total }
});

export const setFilterData = ({
  filterData
}: Pick<ComicsState, 'filterData'>): ComicsActions => ({
  type: 'SET_FILTER_DATA',
  payload: { filterData }
});

export const removeFilter = (): ComicsActions => ({ type: 'REMOVE_FILTER' });

type AddEntryToHideProps = { comicId: number };

export const addEntryToHide = ({
  comicId
}: AddEntryToHideProps): ComicsActions => ({
  type: 'ADD_ENTRY_TO_HIDE',
  payload: { comicId }
});
