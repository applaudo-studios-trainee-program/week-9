import { CharactersState, CharactersActions } from 'reducers/charactersReducer';

export const setData = ({
  data,
  offset,
  total
}: Pick<CharactersState, 'data' | 'offset' | 'total'>): CharactersActions => ({
  type: 'SET_DATA',
  payload: { data, offset, total }
});

export const setFilterData = ({
  filterData
}: Pick<CharactersState, 'filterData'>): CharactersActions => ({
  type: 'SET_FILTER_DATA',
  payload: { filterData }
});

export const removeFilter = (): CharactersActions => ({
  type: 'REMOVE_FILTER'
});

type AddEntryToHideProps = { characterId: number };

export const addEntryToHide = ({
  characterId
}: AddEntryToHideProps): CharactersActions => ({
  type: 'ADD_ENTRY_TO_HIDE',
  payload: { characterId }
});
