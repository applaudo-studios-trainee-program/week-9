import { RouteComponentProps, useParams, withRouter } from 'react-router-dom';
import { useEffect, useRef, useState } from 'react';
import styled from '@emotion/styled';

import { addBookmark, removeBookmark } from 'actions/bookmarks';
import { addEntryToHide } from 'actions/comics';

import { Comic } from 'interfaces/comics';

import { getComicById } from 'services/entities/comics';

import { useBookmarksContext } from 'hooks/useBookmarksContext';
import { useComicsContext } from 'hooks/useComicsContext';

import Button from 'components/Button';
import Column from 'components/Layouts/Alignment/Column';
import Heart from 'assets/icons/Heart';
import ItemList from 'components/ItemList';
import LoadingData from 'assets/icons/LoadingData';
import Remove from 'assets/icons/Remove';
import Row from 'components/Layouts/Alignment/Row';

const StyledImageCard = styled.img`
  border-radius: var(--border-radius);
`;

const ComicDetails = ({ history }: RouteComponentProps) => {
  const isMounted = useRef(false);

  const { id } = useParams<{ id: string }>();

  const [comicData, setComicData] = useState<Comic | {}>({});

  const { dispatchToComics } = useComicsContext();

  const { bookmarksState, dispatchToBookmarks } = useBookmarksContext();
  const { comicBookmarks } = bookmarksState;

  useEffect(() => {
    getComicById({ id }).then(response => {
      if (isMounted.current) setComicData(response);
    });
  }, []);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  }, []);

  const handleBookmark = () => {
    const isBookmarkAlreadyRegistered = !!comicBookmarks.find(
      comicId => comicId === +id
    );

    if (!isBookmarkAlreadyRegistered) {
      dispatchToBookmarks(
        addBookmark({ bookmarkListName: 'comicBookmarks', bookmarkId: +id })
      );
    } else dispatchToBookmarks(removeBookmark({ bookmarkId: +id }));
  };

  const handleHide = () => {
    dispatchToComics(addEntryToHide({ comicId: +id }));

    dispatchToBookmarks(removeBookmark({ bookmarkId: +id }));

    history.push('/list/comics');
  };

  if (!Object.keys(comicData).length) return <LoadingData />;

  const {
    title,
    thumbnail,
    description,
    creatorItems,
    characterItems
  } = comicData as Comic;

  return (
    <Column gap={2}>
      <h1>{title}</h1>

      <StyledImageCard
        alt={`${title} Thumbnail`}
        loading="lazy"
        src={thumbnail}
        title={`${title} Thumbnail`}
      />

      {description ? (
        <Column gap={1} role="contentinfo">
          <h3>Description</h3>

          <span>{description}</span>
        </Column>
      ) : null}

      {creatorItems.length ? (
        <Column gap={1} role="contentinfo">
          <h3>Creator(s)</h3>

          <ItemList dataToList={creatorItems} />
        </Column>
      ) : null}

      {characterItems.length ? (
        <Column gap={1} role="contentinfo">
          <h3>Characters(s)</h3>

          <ItemList dataToList={characterItems} />
        </Column>
      ) : null}

      <Row gap={2}>
        <Button type="button" onClick={handleBookmark} shouldRemoveBackground>
          <Heart
            shouldStaySameColor={
              !!comicBookmarks.find(comicId => comicId === +id)
            }
          />
          <span>
            {comicBookmarks.find(comicId => comicId === +id)
              ? 'Remove from Bookmarks'
              : 'Save'}
          </span>
        </Button>

        <Button type="button" onClick={handleHide} shouldRemoveBackground>
          <Remove />
          <span>Hide</span>
        </Button>
      </Row>
    </Column>
  );
};

export default withRouter(ComicDetails);
