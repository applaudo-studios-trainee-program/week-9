import { RouteComponentProps, useParams, withRouter } from 'react-router-dom';
import { useEffect, useRef, useState } from 'react';
import styled from '@emotion/styled';

import { addBookmark, removeBookmark } from 'actions/bookmarks';
import { addEntryToHide } from 'actions/characters';

import { Character } from 'interfaces/characters';

import { getCharacterById } from 'services/entities/characters';

import { useBookmarksContext } from 'hooks/useBookmarksContext';
import { useCharactersContext } from 'hooks/useCharactersContext';

import Button from 'components/Button';
import Column from 'components/Layouts/Alignment/Column';
import Heart from 'assets/icons/Heart';
import ItemList from 'components/ItemList';
import LoadingData from 'assets/icons/LoadingData';
import Remove from 'assets/icons/Remove';
import Row from 'components/Layouts/Alignment/Row';

const StyledImageCard = styled.img`
  border-radius: var(--border-radius);
`;

const CharacterDetails = ({ history }: RouteComponentProps) => {
  const isMounted = useRef(false);

  const { id } = useParams<{ id: string }>();

  const [characterData, setCharacterData] = useState<Character | {}>({});

  const { dispatchToCharacters } = useCharactersContext();

  const { bookmarksState, dispatchToBookmarks } = useBookmarksContext();
  const { characterBookmarks } = bookmarksState;

  useEffect(() => {
    getCharacterById({ id }).then(response => {
      if (isMounted.current) setCharacterData(response);
    });
  }, []);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  }, []);

  const handleBookmark = () => {
    const isBookmarkAlreadyRegistered = !!characterBookmarks.find(
      charaterId => charaterId === +id
    );

    if (!isBookmarkAlreadyRegistered) {
      dispatchToBookmarks(
        addBookmark({ bookmarkListName: 'characterBookmarks', bookmarkId: +id })
      );
    } else dispatchToBookmarks(removeBookmark({ bookmarkId: +id }));
  };

  const handleHide = () => {
    dispatchToCharacters(addEntryToHide({ characterId: +id }));

    dispatchToBookmarks(removeBookmark({ bookmarkId: +id }));

    history.push('/list/characters');
  };

  if (!Object.keys(characterData).length) return <LoadingData />;

  const {
    name,
    thumbnail,
    description,
    comicsItems,
    storiesItems
  } = characterData as Character;

  return (
    <Column gap={2}>
      <h1>{name}</h1>

      <StyledImageCard
        alt={`${name} Thumbnail`}
        loading="lazy"
        src={thumbnail}
        title={`${name} Thumbnail`}
      />

      {description ? (
        <Column gap={1} role="contentinfo">
          <h3>Description</h3>

          <span>{description}</span>
        </Column>
      ) : null}

      {comicsItems.length ? (
        <Column gap={1} role="contentinfo">
          <h3>Comics</h3>

          <ItemList dataToList={comicsItems} />
        </Column>
      ) : null}

      {storiesItems.length ? (
        <Column gap={1} role="contentinfo">
          <h3>Stories</h3>

          <ItemList dataToList={storiesItems} />
        </Column>
      ) : null}

      <Row gap={2}>
        <Button type="button" onClick={handleBookmark} shouldRemoveBackground>
          <Heart
            shouldStaySameColor={
              !!characterBookmarks.find(charaterId => charaterId === +id)
            }
          />
          <span>
            {characterBookmarks.find(charaterId => charaterId === +id)
              ? 'Remove from Bookmarks'
              : 'Save'}
          </span>
        </Button>

        <Button type="button" onClick={handleHide} shouldRemoveBackground>
          <Remove />
          <span>Hide</span>
        </Button>
      </Row>
    </Column>
  );
};

export default withRouter(CharacterDetails);
