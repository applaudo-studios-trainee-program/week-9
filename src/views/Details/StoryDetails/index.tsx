import { RouteComponentProps, useParams, withRouter } from 'react-router-dom';
import { useEffect, useRef, useState } from 'react';

import { addBookmark, removeBookmark } from 'actions/bookmarks';

import { addEntryToHide } from 'actions/stories';
import { getStoryById } from 'services/entities/stories';

import { Story } from 'interfaces/stories';

import { useBookmarksContext } from 'hooks/useBookmarksContext';
import { useStoriesContext } from 'hooks/useStoriesContext';

import Button from 'components/Button';
import Column from 'components/Layouts/Alignment/Column';
import Heart from 'assets/icons/Heart';
import ItemList from 'components/ItemList';
import LoadingData from 'assets/icons/LoadingData';
import Remove from 'assets/icons/Remove';
import Row from 'components/Layouts/Alignment/Row';

const StoryDetails = ({ history }: RouteComponentProps) => {
  const isMounted = useRef(false);

  const { id } = useParams<{ id: string }>();

  const [storyData, setStoryData] = useState<Story | {}>({});

  const { dispatchToStories } = useStoriesContext();

  const { bookmarksState, dispatchToBookmarks } = useBookmarksContext();
  const { storiesBookmarks } = bookmarksState;

  useEffect(() => {
    getStoryById({ id }).then(response => {
      if (isMounted.current) setStoryData(response);
    });
  }, []);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  }, []);

  const handleBookmark = () => {
    const isBookmarkAlreadyRegistered = !!storiesBookmarks.find(
      storiesId => storiesId === +id
    );

    if (!isBookmarkAlreadyRegistered) {
      dispatchToBookmarks(
        addBookmark({ bookmarkListName: 'storiesBookmarks', bookmarkId: +id })
      );
    } else dispatchToBookmarks(removeBookmark({ bookmarkId: +id }));
  };

  const handleHide = () => {
    dispatchToStories(addEntryToHide({ storyId: +id }));

    dispatchToBookmarks(removeBookmark({ bookmarkId: +id }));

    history.push('/list/stories');
  };

  if (!Object.keys(storyData).length) return <LoadingData />;

  const { title, description, comicItems } = storyData as Story;

  return (
    <Column gap={2}>
      <h1>{title}</h1>

      {description ? (
        <Column gap={1} role="contentinfo">
          <h3>Description</h3>

          <span>{description}</span>
        </Column>
      ) : null}

      {comicItems?.length ? (
        <Column gap={1} role="contentinfo">
          <h3>Comics</h3>

          <ItemList dataToList={comicItems} />
        </Column>
      ) : null}

      <Row gap={2}>
        <Button type="button" onClick={handleBookmark} shouldRemoveBackground>
          <Heart
            shouldStaySameColor={
              !!storiesBookmarks.find(storyId => storyId === +id)
            }
          />
          <span>
            {storiesBookmarks.find(storyId => storyId === +id)
              ? 'Remove from Bookmarks'
              : 'Save'}
          </span>
        </Button>

        <Button type="button" onClick={handleHide} shouldRemoveBackground>
          <Remove />
          <span>Hide</span>
        </Button>
      </Row>
    </Column>
  );
};

export default withRouter(StoryDetails);
