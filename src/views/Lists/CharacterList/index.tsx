import { ChangeEvent, useCallback, useEffect, useRef, useState } from 'react';

import { debounce } from 'lodash';

import Select, { ActionMeta, OptionTypeBase, ValueType } from 'react-select';

import { CharactersFilters } from 'reducers/charactersReducer';

import { filterCharactersOptions } from 'data';

import { getCharacters } from 'services/entities/characters';
import { getComicsIdAndName } from 'services/entities/comics';
import { getStoriesIdAndName } from 'services/entities/stories';

import { removeFilter, setData, setFilterData } from 'actions/characters';

import { useCharactersContext } from 'hooks/useCharactersContext';

import Column from 'components/Layouts/Alignment/Column';
import GenericPagination from 'components/GenericPagination';
import LoadingData from 'assets/icons/LoadingData';
import Row from 'components/Layouts/Alignment/Row';
import Search from 'components/Forms/Search';

type Options = { value: string; label: string }[];

const CharacterList = () => {
  const isMounted = useRef(false);

  const [comicOptionList, setComicOptionList] = useState<Options>([]);
  const [storyOptionList, setStoryOptionList] = useState<Options>([]);
  const [inputValue, setInputValue] = useState('');

  const { charactersState, dispatchToCharacters } = useCharactersContext();

  const { data, filterData, offset } = charactersState;

  useEffect(() => {
    getCharacters({ offset: 0 }).then(
      response =>
        isMounted.current &&
        dispatchToCharacters(setData({ ...response, offset: 1 }))
    );

    getComicsIdAndName().then(
      response => isMounted.current && setComicOptionList(response)
    );

    getStoriesIdAndName().then(
      response => isMounted.current && setStoryOptionList(response)
    );

    return () => {
      dispatchToCharacters(removeFilter());
    };
  }, []);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  }, []);

  const handleDebouncedSearch = useCallback(
    debounce((filter: Required<CharactersFilters>) => {
      getCharacters({ offset: 0, filter }).then(
        response =>
          isMounted.current &&
          dispatchToCharacters(setData({ ...response, offset: 1 }))
      );
    }, 1000),
    []
  );

  const handlePageChange = (page: number) => {
    if (filterData) {
      getCharacters({
        offset: (page - 1) * 20,
        filter: filterData as Required<CharactersFilters>
      }).then(
        response =>
          isMounted.current &&
          dispatchToCharacters(setData({ ...response, offset: page }))
      );
    } else {
      getCharacters({ offset: (page - 1) * 20 }).then(
        response =>
          isMounted.current &&
          dispatchToCharacters(setData({ ...response, offset: page }))
      );
    }
  };

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    const input = event.target as HTMLInputElement;
    const inputValue = input.value.trim();

    setInputValue(input.value);

    if (inputValue !== '') {
      dispatchToCharacters(
        setFilterData({ filterData: { filterValue: inputValue } })
      );

      handleDebouncedSearch(filterData as Required<CharactersFilters>);
    }
  };

  const handleFilterOption = (
    selectedValue: ValueType<OptionTypeBase, boolean>
  ) => {
    if (selectedValue) {
      const { value } = selectedValue as {
        value: 'nameStartsWith' | 'comics' | 'stories';
      };

      dispatchToCharacters(
        setFilterData({ filterData: { filterName: value } })
      );
    } else {
      dispatchToCharacters(removeFilter());

      getCharacters({ offset: 0 }).then(
        response =>
          isMounted.current &&
          dispatchToCharacters(setData({ ...response, offset: 1 }))
      );

      if (filterData?.filterName === 'nameStartsWith') setInputValue('');
    }
  };

  const handleFilterSelection = (
    selectionValue: ValueType<OptionTypeBase, false>,
    actionMeta: ActionMeta<OptionTypeBase>
  ) => {
    const { name } = actionMeta;
    const { value } = selectionValue as { value: string };

    if (name === 'comics') {
      dispatchToCharacters(
        setFilterData({ filterData: { filterValue: value } })
      );
    } else {
      dispatchToCharacters(
        setFilterData({ filterData: { filterValue: value } })
      );
    }

    const filterInfo: CharactersFilters = { ...filterData, filterValue: value };

    getCharacters({
      offset: 0,
      filter: filterInfo as Required<CharactersFilters>
    }).then(
      response =>
        isMounted.current &&
        dispatchToCharacters(setData({ ...response, offset }))
    );
  };

  if (!isMounted.current) {
    return !data.length ? <LoadingData /> : null;
  }

  return (
    <Column gap={2}>
      <h1>Character List</h1>

      <Row gap={3}>
        <Column flex gap={1}>
          <form data-testid="filterForm">
            <label htmlFor="filterOptions">
              Filter Options
              <Select
                name="filterOptions"
                inputId="filterOptions"
                defaultValue={filterCharactersOptions[0]}
                options={filterCharactersOptions}
                isClearable
                onChange={handleFilterOption}
              />
            </label>
          </form>
        </Column>

        {filterData ? (
          <Column flex gap={1}>
            <form data-testid="filtersSelectionForm">
              {filterData.filterName === 'nameStartsWith' ? (
                <label htmlFor="charactersName">
                  By Name
                  <Search
                    name="charactersName"
                    inputId="charactersName"
                    value={inputValue}
                    placeholder="Search for Characters"
                    onChange={handleInputChange}
                  />
                </label>
              ) : null}

              {filterData.filterName === 'comics' ? (
                <label htmlFor="charactersComics">
                  By Comics
                  <Select
                    inputId="charactersComics"
                    name="charactersComics"
                    options={comicOptionList}
                    onChange={handleFilterSelection}
                  />
                </label>
              ) : null}

              {filterData.filterName === 'stories' ? (
                <label htmlFor="charactersStories">
                  By Stories
                  <Select
                    inputId="charactersStories"
                    name="charactersStories"
                    options={storyOptionList}
                    onChange={handleFilterSelection}
                  />
                </label>
              ) : null}
            </form>
          </Column>
        ) : null}
      </Row>

      <GenericPagination
        {...charactersState}
        type="characters"
        limit={20}
        onPageChange={handlePageChange}
      />
    </Column>
  );
};

export default CharacterList;
