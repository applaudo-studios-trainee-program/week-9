import { ChangeEvent, useCallback, useEffect, useRef, useState } from 'react';

import { debounce } from 'lodash';

import Select, { OptionTypeBase, ValueType } from 'react-select';

import { comicFormats, filterComicsOptions } from 'data';

import { ComicsFilters } from 'reducers/comicsReducer';

import { getComics } from 'services/entities/comics';

import { setData, setFilterData, removeFilter } from 'actions/comics';

import { useComicsContext } from 'hooks/useComicsContext';

import Column from 'components/Layouts/Alignment/Column';
import GenericPagination from 'components/GenericPagination';
import LoadingData from 'assets/icons/LoadingData';
import Row from 'components/Layouts/Alignment/Row';
import Search from 'components/Forms/Search';

const ComicList = () => {
  const isMounted = useRef(false);

  const [inputValue, setInputValue] = useState('');

  const { comicsState, dispatchToComics } = useComicsContext();

  const { data, offset, filterData } = comicsState;

  useEffect(() => {
    getComics({ offset: 0 }).then(
      response =>
        isMounted.current &&
        dispatchToComics(setData({ ...response, offset: 1 }))
    );

    return () => {
      dispatchToComics(removeFilter());
    };
  }, []);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  }, []);

  const handleDebouncedSearch = useCallback(
    debounce((filter: Required<ComicsFilters>) => {
      getComics({ offset: 0, filter }).then(
        response =>
          isMounted.current &&
          dispatchToComics(setData({ ...response, offset: 1 }))
      );
    }, 1000),
    []
  );

  const handlePageChange = (page: number) => {
    if (filterData) {
      getComics({
        offset: (page - 1) * 20,
        filter: filterData as Required<ComicsFilters>
      }).then(
        response =>
          isMounted.current &&
          dispatchToComics(setData({ ...response, offset: page }))
      );
    } else {
      getComics({ offset: (page - 1) * 20 }).then(
        response =>
          isMounted.current &&
          dispatchToComics(setData({ ...response, offset: page }))
      );
    }
  };

  const handleFilterOption = (
    selectedValue: ValueType<OptionTypeBase, boolean>
  ) => {
    if (selectedValue) {
      const { value } = selectedValue as {
        value: 'format' | 'titleStartsWith';
      };

      dispatchToComics(setFilterData({ filterData: { filterName: value } }));
    } else {
      dispatchToComics(removeFilter());

      getComics({ offset: 0 }).then(
        response =>
          isMounted.current &&
          dispatchToComics(setData({ ...response, offset: 1 }))
      );

      if (filterData?.filterName === 'titleStartsWith') setInputValue('');
    }
  };

  const handleComicFormatSelection = (
    selectedValue: ValueType<OptionTypeBase, boolean>
  ) => {
    const { value } = selectedValue as { value: string };

    dispatchToComics(setFilterData({ filterData: { filterValue: value } }));

    const filterInfo: ComicsFilters = { ...filterData, filterValue: value };

    getComics({
      offset: 0,
      filter: filterInfo as Required<ComicsFilters>
    }).then(
      response =>
        isMounted.current && dispatchToComics(setData({ ...response, offset }))
    );
  };

  const handleComicTitleChange = ({
    target
  }: ChangeEvent<HTMLInputElement>) => {
    const input = target as HTMLInputElement;
    const inputValue = input.value.trim();

    setInputValue(input.value);

    dispatchToComics(
      setFilterData({ filterData: { filterValue: inputValue } })
    );

    if (inputValue !== '') {
      handleDebouncedSearch(filterData as Required<ComicsFilters>);
    }
  };

  if (!isMounted.current) {
    return !data.length ? <LoadingData /> : null;
  }

  return (
    <Column gap={2}>
      <h1>Comic List</h1>

      <Row gap={3}>
        <Column flex>
          <form data-testid="filterForm">
            <label htmlFor="filterOptions">
              Filter Options
              <Select
                defaultValue={filterComicsOptions[0]}
                name="filterOptions"
                inputId="filterOptions"
                options={filterComicsOptions}
                isClearable
                onChange={handleFilterOption}
              />
            </label>
          </form>
        </Column>

        {filterData ? (
          <Column flex>
            <form data-testid="filtersSelectionForm">
              {filterData.filterName === 'format' ? (
                <label htmlFor="comicsFormats">
                  By Format
                  <Select
                    name="comicsFormats"
                    inputId="comicsFormats"
                    defaultValue={comicFormats[0]}
                    options={comicFormats}
                    isClearable
                    onChange={handleComicFormatSelection}
                  />
                </label>
              ) : null}

              {filterData.filterName === 'titleStartsWith' ? (
                <label htmlFor="comicsTitle">
                  By Title
                  <Search
                    name="comicsTitle"
                    inputId="comicsTitle"
                    value={inputValue}
                    onChange={handleComicTitleChange}
                  />
                </label>
              ) : null}
            </form>
          </Column>
        ) : null}
      </Row>

      <GenericPagination
        {...comicsState}
        type="comics"
        limit={20}
        onPageChange={handlePageChange}
      />
    </Column>
  );
};

export default ComicList;
