import { ChangeEvent, useCallback, useEffect, useRef, useState } from 'react';

import { debounce } from 'lodash';

import Select, { OptionTypeBase, ValueType } from 'react-select';

import { filterStoriesOptions } from 'data';

import { getStories } from 'services/entities/stories';

import { removeFilter, setData, setFilterData } from 'actions/stories';

import { StoriesFilters } from 'reducers/storiesReducer';

import { useStoriesContext } from 'hooks/useStoriesContext';

import Column from 'components/Layouts/Alignment/Column';
import DatePicker from 'components/Forms/DatePicker';
import GenericPagination from 'components/GenericPagination';
import LoadingData from 'assets/icons/LoadingData';
import Row from 'components/Layouts/Alignment/Row';

const StoryList = () => {
  const isMounted = useRef(false);

  const [inputValue, setInputValue] = useState('');

  const { storiesState, dispatchToStories } = useStoriesContext();

  const { data, filterData } = storiesState;

  useEffect(() => {
    getStories({ offset: 0 }).then(
      response =>
        isMounted.current &&
        dispatchToStories(setData({ ...response, offset: 1 }))
    );

    return () => {
      dispatchToStories(removeFilter());
    };
  }, []);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  }, []);

  const handleDebouncedSearch = useCallback(
    debounce((filter: Required<StoriesFilters>) => {
      getStories({ offset: 0, filter }).then(
        response =>
          isMounted.current &&
          dispatchToStories(setData({ ...response, offset: 1 }))
      );
    }, 1000),
    []
  );

  const handlePageChange = (page: number) => {
    if (filterData) {
      getStories({
        offset: (page - 1) * 20,
        filter: filterData as Required<StoriesFilters>
      }).then(
        response =>
          isMounted.current &&
          dispatchToStories(setData({ ...response, offset: page }))
      );
    } else {
      getStories({ offset: (page - 1) * 20 }).then(
        response =>
          isMounted.current &&
          dispatchToStories(setData({ ...response, offset: page }))
      );
    }
  };

  const handleDateChange = ({ target }: ChangeEvent<HTMLInputElement>) => {
    const input = target as HTMLInputElement;
    const inputValue = input.value;

    setInputValue(inputValue);

    dispatchToStories(
      setFilterData({ filterData: { filterValue: inputValue } })
    );

    const filterInfo: StoriesFilters = {
      ...filterData,
      filterValue: inputValue
    };

    handleDebouncedSearch(filterInfo as Required<StoriesFilters>);
  };

  const handleFilterOption = (
    selectedValue: ValueType<OptionTypeBase, boolean>
  ) => {
    if (selectedValue) {
      const { value } = selectedValue as {
        value: 'modifiedSince';
      };

      dispatchToStories(setFilterData({ filterData: { filterName: value } }));
    } else {
      dispatchToStories(removeFilter());

      getStories({ offset: 0 }).then(
        response =>
          isMounted.current &&
          dispatchToStories(setData({ ...response, offset: 1 }))
      );
    }
  };

  if (!isMounted.current) {
    return !data.length ? <LoadingData /> : null;
  }

  return (
    <Column gap={2}>
      <h1>Stories List</h1>

      <Row gap={3}>
        <Column flex gap={1}>
          <form data-testid="filterForm">
            <label htmlFor="filterOptions">
              Filter Options
              <Select
                name="filterOptions"
                inputId="filterOptions"
                defaultValue={filterStoriesOptions[0]}
                options={filterStoriesOptions}
                isClearable
                onChange={handleFilterOption}
              />
            </label>
          </form>
        </Column>

        {filterData ? (
          <Column flex>
            <form data-testid="filtersSelectionForm">
              {filterData.filterName === 'modifiedSince' ? (
                <label htmlFor="storiesDate">
                  By Date
                  <DatePicker
                    inputId="storiesDate"
                    name="storiesDate"
                    value={inputValue}
                    onChange={handleDateChange}
                  />
                </label>
              ) : null}
            </form>
          </Column>
        ) : null}
      </Row>

      <GenericPagination
        {...storiesState}
        type="stories"
        limit={20}
        onPageChange={handlePageChange}
      />
    </Column>
  );
};

export default StoryList;
