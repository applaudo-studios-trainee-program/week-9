import { useEffect, useRef, useState } from 'react';
import { Link } from 'react-router-dom';

import { Characters } from 'interfaces/characters';
import { Comics } from 'interfaces/comics';
import { Stories } from 'interfaces/stories';

import { getCharacterById } from 'services/entities/characters';
import { getComicById } from 'services/entities/comics';
import { getStoryById } from 'services/entities/stories';

import { removeAllBookmarks } from 'actions/bookmarks';

import { useBookmarksContext } from 'hooks/useBookmarksContext';

import Button from 'components/Button';
import Column from 'components/Layouts/Alignment/Column';
import Container from 'components/Container';
import Row from 'components/Layouts/Alignment/Row';

const Bookmarks = () => {
  const isMounted = useRef(false);

  const [characters, setCharacters] = useState<Characters[]>([]);
  const [comics, setComics] = useState<Comics[]>([]);
  const [stories, setStories] = useState<Stories[]>([]);

  const { bookmarksState, dispatchToBookmarks } = useBookmarksContext();

  const {
    characterBookmarks,
    comicBookmarks,
    storiesBookmarks
  } = bookmarksState;

  useEffect(() => {
    characterBookmarks.forEach(characterBookmark =>
      getCharacterById({
        id: characterBookmark.toString()
      }).then(response => {
        if (isMounted.current) {
          setCharacters(characters => [...characters, response]);
        }
      })
    );

    comicBookmarks.map(comicBookmark =>
      getComicById({ id: comicBookmark.toString() }).then(response => {
        if (isMounted.current) setComics(comics => [...comics, response]);
      })
    );

    storiesBookmarks.map(comicBookmark =>
      getStoryById({ id: comicBookmark.toString() }).then(response => {
        if (isMounted.current) setStories(stories => [...stories, response]);
      })
    );
  }, []);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  }, []);

  const handleRemove = () => dispatchToBookmarks(removeAllBookmarks());

  return (
    <Container>
      <Column gap={3}>
        <h1>Bookmarks</h1>

        {!characterBookmarks.length &&
        !comicBookmarks.length &&
        !storiesBookmarks.length ? (
          <span>
            There are <strong>no bookmarks</strong> yet, but you can save any
            data that you like 😃
          </span>
        ) : null}

        {characterBookmarks.length ? (
          <Column gap={2}>
            <h2>Characters Bookmarks</h2>

            <ul>
              <Row gap={1.5}>
                {characters.map(character => (
                  <li key={character.id}>
                    <Link to={`/details/characters/${character.id}`}>
                      {character.name}
                    </Link>
                  </li>
                ))}
              </Row>
            </ul>
          </Column>
        ) : null}

        {comicBookmarks.length ? (
          <Column gap={2}>
            <h2>Comics Bookmarks</h2>

            <ul>
              <Row gap={1.5}>
                {comics.map(comic => (
                  <li key={comic.id}>
                    <Link to={`/details/comics/${comic.id}`}>
                      {comic.title}
                    </Link>
                  </li>
                ))}
              </Row>
            </ul>
          </Column>
        ) : null}

        {storiesBookmarks.length ? (
          <Column gap={2}>
            <h2>Stories Bookmarks</h2>

            <ul>
              <Row gap={1.5}>
                {stories.map(story => (
                  <li key={story.id}>
                    <Link to={`/details/stories/${story.id}`}>
                      {story.title}
                    </Link>
                  </li>
                ))}
              </Row>
            </ul>
          </Column>
        ) : null}

        {characterBookmarks.length ||
        comicBookmarks.length ||
        storiesBookmarks.length ? (
          <Button type="button" onClick={handleRemove}>
            Remove all Bookmarks
          </Button>
        ) : null}
      </Column>
    </Container>
  );
};

export default Bookmarks;
