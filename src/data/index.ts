export const filterCharactersOptions = [
  { value: '', label: 'Select an option', isDisabled: true },
  { value: 'nameStartsWith', label: 'Name' },
  { value: 'comics', label: 'Comics' },
  { value: 'stories', label: 'Stories' }
];

export const filterComicsOptions = [
  { value: '', label: 'Select an option', isDisabled: true },
  { value: 'format', label: 'Format' },
  { value: 'titleStartsWith', label: 'Title' }
];

export const comicFormats = [
  { value: '', label: 'Select an option', isDisabled: true },
  { value: 'comic', label: 'Comic' },
  { value: 'trade paperback', label: 'Trade Paperback' },
  { value: 'hardcover', label: 'Hardcover' },
  { value: 'digest', label: 'Digest' },
  { value: 'graphic novel', label: 'Graphic Novel' },
  { value: 'digital comic', label: 'Digital Comic' },
  { value: 'infinite comic', label: 'Infinite Comic' }
];

export const filterStoriesOptions = [
  { value: '', label: 'Select an option', isDisabled: true },
  { value: 'modifiedSince', label: 'Last Modified' }
];
