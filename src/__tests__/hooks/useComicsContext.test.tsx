import { ReactNode } from 'react';
import { renderHook } from '@testing-library/react-hooks';

import { ComicsContextProvider } from 'contexts/ComicsContext';

import { ComicsState } from 'reducers/comicsReducer';

import { useComicsContext } from 'hooks/useComicsContext';

describe('tests to the useCharactersContext custom hook', () => {
  test('should return the initial state by default', () => {
    const initialState: ComicsState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const wrapper = ({ children }: { children: ReactNode }) => (
      <ComicsContextProvider>{children}</ComicsContextProvider>
    );

    const { result } = renderHook(() => useComicsContext(), { wrapper });

    expect(result.current.comicsState).toEqual(initialState);
  });

  test('should throw an error if the hook is used outside of a provider', () => {
    try {
      renderHook(useComicsContext);
    } catch (error) {
      expect(error).toBe(
        /useComicsContext must be used within a ComicsProvider/i
      );
    }
  });
});
