import { ReactNode } from 'react';
import { renderHook } from '@testing-library/react-hooks';

import { CharactersContextProvider } from 'contexts/CharactersContext';

import { CharactersState } from 'reducers/charactersReducer';

import { useCharactersContext } from 'hooks/useCharactersContext';

describe('tests to the useCharactersContext custom hook', () => {
  test('should return the initial state by default', () => {
    const initialState: CharactersState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const wrapper = ({ children }: { children: ReactNode }) => (
      <CharactersContextProvider>{children}</CharactersContextProvider>
    );

    const { result } = renderHook(() => useCharactersContext(), { wrapper });

    expect(result.current.charactersState).toEqual(initialState);
  });

  test('should throw an error if the hook is used outside of a provider', () => {
    try {
      renderHook(useCharactersContext);
    } catch (error) {
      expect(error).toBe(
        /useCharactersContext must be used within a CharactersProvider/i
      );
    }
  });
});
