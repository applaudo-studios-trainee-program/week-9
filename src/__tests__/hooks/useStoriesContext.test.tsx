import { ReactNode } from 'react';
import { renderHook } from '@testing-library/react-hooks';

import { StoriesContextProvider } from 'contexts/StoriesContext';

import { StoriesState } from 'reducers/storiesReducer';

import { useStoriesContext } from 'hooks/useStoriesContext';

describe('tests to the useCharactersContext custom hook', () => {
  test('should return the initial state by default', () => {
    const initialState: StoriesState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const wrapper = ({ children }: { children: ReactNode }) => (
      <StoriesContextProvider>{children}</StoriesContextProvider>
    );

    const { result } = renderHook(() => useStoriesContext(), { wrapper });

    expect(result.current.storiesState).toEqual(initialState);
  });

  test('should throw an error if the hook is used outside of a provider', () => {
    try {
      renderHook(useStoriesContext);
    } catch (error) {
      expect(error).toBe(
        /useStoriesContext must be used within a StoriesProvider/i
      );
    }
  });
});
