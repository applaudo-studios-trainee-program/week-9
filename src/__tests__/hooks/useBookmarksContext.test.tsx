import { Dispatch, ReactNode } from 'react';
import { renderHook } from '@testing-library/react-hooks';

import { BookmarksContext } from 'contexts/BookmarksContext';

import { useBookmarksContext } from 'hooks/useBookmarksContext';
import { initialBookmarksState } from 'components/App';
import { BookmarksActions } from 'reducers/bookmarksReducer';

const dispatchToBookmarks = jest.fn<Dispatch<BookmarksActions>, []>();

describe('tests to the useCharactersContext custom hook', () => {
  test('should return the initial state by default', () => {
    const wrapper = ({ children }: { children: ReactNode }) => (
      <BookmarksContext.Provider
        value={{ bookmarksState: initialBookmarksState, dispatchToBookmarks }}
      >
        {children}
      </BookmarksContext.Provider>
    );

    const { result } = renderHook(() => useBookmarksContext(), { wrapper });

    expect(result.current.bookmarksState).toEqual(initialBookmarksState);
  });

  test('should throw an error if the hook is used outside of a provider', () => {
    try {
      renderHook(useBookmarksContext);
    } catch (error) {
      expect(error).toBe(
        /useBookmarksContext must be used within a BookmarksProvider/i
      );
    }
  });
});
