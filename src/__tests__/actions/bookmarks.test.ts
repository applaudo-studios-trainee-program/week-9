import {
  addBookmark,
  removeAllBookmarks,
  removeBookmark
} from 'actions/bookmarks';
import { BookmarksTypes, BookmarksActions } from 'reducers/bookmarksReducer';
import { initialBookmarksState } from 'components/App';

const setStorageMock = jest.spyOn(Storage.prototype, 'setItem');
const getStorageMock = jest.spyOn(Storage.prototype, 'getItem');

beforeEach(() => {
  localStorage.setItem('bookmarks', JSON.stringify(initialBookmarksState));
});

afterEach(() => {
  jest.clearAllMocks();

  localStorage.clear();
});

describe('tests to the bookmarks action creators', () => {
  test('should call the local storage to get and set when a bookmark is added', () => {
    const actionData: {
      bookmarkId: number;
      bookmarkListName: BookmarksTypes;
    } = {
      bookmarkId: 959,
      bookmarkListName: 'storiesBookmarks'
    };

    const actionToBeEqual: BookmarksActions = {
      type: 'ADD_BOOKMARK',
      payload: { ...actionData }
    };

    const action = addBookmark({ ...actionData });

    expect(getStorageMock).toBeCalledWith('bookmarks');
    expect(setStorageMock).toBeCalled();

    expect(action).toEqual(actionToBeEqual);
  });

  test('should call the local storage to get and set when a bookmark is removed', () => {
    const actionData: { bookmarkId: number } = {
      bookmarkId: 6
    };

    const actionToBeEqual: BookmarksActions = {
      type: 'REMOVE_BOOKMARK',
      payload: actionData
    };

    const action = removeBookmark({ bookmarkId: 6 });

    expect(getStorageMock).toBeCalledWith('bookmarks');
    expect(setStorageMock).toBeCalled();

    expect(action).toEqual(actionToBeEqual);
  });

  test('should call the local storage to set the initialState when all the bookmarks are removed', () => {
    const actionToBeEqual: BookmarksActions = { type: 'REMOVE_ALL_BOOKMARKS' };

    const action = removeAllBookmarks();

    expect(setStorageMock).toBeCalledWith(
      'bookmarks',
      JSON.stringify(initialBookmarksState)
    );

    expect(action).toEqual(actionToBeEqual);
  });
});
