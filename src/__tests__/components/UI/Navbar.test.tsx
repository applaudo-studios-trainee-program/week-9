import { screen } from '@testing-library/react';

import { renderWithRouter } from '__utils__';

import Navbar from 'components/UI/Navbar';
import userEvent from '@testing-library/user-event';

describe('tests to the Navbar UI component', () => {
  test('should change the pathname when a anchor is clicked', () => {
    renderWithRouter(<Navbar />);

    expect(window.location.pathname).toBe('/');

    const burgerMenu = screen.getByRole('switch');

    userEvent.click(burgerMenu);

    const navbarAnchor = screen.getByRole('link', { name: /characters/i });

    userEvent.click(navbarAnchor);

    expect(window.location.pathname).toBe('/list/characters');
  });

  test('should change the pathname when the heading is clicked', () => {
    renderWithRouter(<Navbar />, { route: '/list/characters' });

    expect(window.location.pathname).toBe('/list/characters');

    const navbarHeading = screen.getByRole('link', { name: /marvel docs/i });

    userEvent.click(navbarHeading);

    expect(window.location.pathname).toBe('/');
  });

  test('should the view list be hidden by default but show it when the burger menu is clicked', () => {
    renderWithRouter(<Navbar />);

    expect(screen.getByRole('list', { hidden: true })).not.toBeVisible();

    const burgerMenu = screen.getByRole('switch');

    userEvent.click(burgerMenu);

    expect(screen.getByRole('list', { hidden: false })).toBeVisible();
  });
});
