import { screen } from '@testing-library/react';

import { renderWithRouter } from '__utils__';

import App from 'components/App';

describe('tests to the App component', () => {
  test('should render the Navbar component', () => {
    renderWithRouter(<App />);

    expect(screen.getByRole('banner')).toBeInTheDocument();
  });

  test('should render the Home view component by default', () => {
    renderWithRouter(<App />);

    expect(window.location.pathname).toBe('/');
  });

  test('should render the List View layout', () => {
    renderWithRouter(<App />, { route: '/list/characters' });

    expect(window.location.pathname).toBe('/list/characters');
  });

  test('should render the Details View layout', () => {
    renderWithRouter(<App />, { route: '/details/characters/1017100' });

    expect(window.location.pathname).toBe('/details/characters/1017100');
  });

  test('should render the Details View layout', () => {
    renderWithRouter(<App />, { route: '/bookmarks' });

    expect(window.location.pathname).toBe('/bookmarks');
  });
});
