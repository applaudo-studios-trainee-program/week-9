import { Suspense } from 'react';
import { Route } from 'react-router-dom';

import { Paths } from 'components/App';

import { renderWithRouter } from '__utils__';

import ListsLayout from 'components/Layouts/ListsLayout';

import Spinner from 'assets/icons/Spinner';

describe('tests to the ListsLayout layout component', () => {
  test('should change the pathname to the character list path', () => {
    renderWithRouter(
      <Suspense fallback={<Spinner />}>
        <Route path={Paths.ListsPath} component={ListsLayout} />
      </Suspense>,
      { route: '/list/characters' }
    );

    expect(window.location.pathname).toBe('/list/characters');
  });

  test('should change the pathname to the comic list path', () => {
    renderWithRouter(
      <Suspense fallback={<Spinner />}>
        <Route path={Paths.ListsPath} component={ListsLayout} />
      </Suspense>,
      { route: '/list/comics' }
    );

    expect(window.location.pathname).toBe('/list/comics');
  });

  test('should change the pathname to the story list path', () => {
    renderWithRouter(
      <Suspense fallback={<Spinner />}>
        <Route path={Paths.ListsPath} component={ListsLayout} />
      </Suspense>,
      { route: '/list/stories' }
    );

    expect(window.location.pathname).toBe('/list/stories');
  });
});
