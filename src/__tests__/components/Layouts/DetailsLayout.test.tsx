import { Suspense } from 'react';
import { Route } from 'react-router-dom';

import { Paths } from 'components/App';

import { renderWithRouter } from '__utils__';

import DetailsLayout from 'components/Layouts/DetailsLayout';

import Spinner from 'assets/icons/Spinner';

describe('tests to the DetailsLayout layout component', () => {
  test('should change the pathname to the characters details path', () => {
    renderWithRouter(
      <Suspense fallback={<Spinner />}>
        <Route path={Paths.DetailsPath} component={DetailsLayout} />
      </Suspense>,
      { route: '/details/characters/1017100' }
    );

    expect(window.location.pathname).toBe('/details/characters/1017100');
  });

  test('should change the pathname to the comics details path', () => {
    renderWithRouter(
      <Suspense fallback={<Spinner />}>
        <Route path={Paths.DetailsPath} component={DetailsLayout} />
      </Suspense>,
      { route: '/details/comics/40632' }
    );

    expect(window.location.pathname).toBe('/details/comics/40632');
  });

  test('should change the pathname to the comics details path', () => {
    renderWithRouter(
      <Suspense fallback={<Spinner />}>
        <Route path={Paths.DetailsPath} component={DetailsLayout} />
      </Suspense>,
      { route: '/details/stories/744' }
    );

    expect(window.location.pathname).toBe('/details/stories/744');
  });
});
