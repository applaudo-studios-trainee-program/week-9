import { Dispatch } from 'react';
import { rest } from 'msw';
import { Route } from 'react-router-dom';
import { screen } from '@testing-library/react';
import { setupServer } from 'msw/node';

import { removeAllBookmarks } from 'actions/bookmarks';

import { BookmarksActions } from 'reducers/bookmarksReducer';

import { BookmarksContext } from 'contexts/BookmarksContext';

import { getAPIResponse, getBaseUrl, renderWithRouter } from '__utils__';

import { initialBookmarksState, Paths } from 'components/App';

import { singleCharacterDataMock } from '__mocks__/characters';
import { singleComicDataMock } from '__mocks__/comics';
import { singleStoryDataMock } from '__mocks__/stories';

import Bookmarks from 'views/Bookmarks';
import userEvent from '@testing-library/user-event';

const server = setupServer(
  rest.get(getBaseUrl('characters/:id'), (_, res, ctx) =>
    res(ctx.status(200), ctx.json(getAPIResponse(singleCharacterDataMock)))
  ),
  rest.get(getBaseUrl('comics/:id'), (_, res, ctx) =>
    res(ctx.status(200), ctx.json(getAPIResponse(singleComicDataMock)))
  ),
  rest.get(getBaseUrl('stories/:id'), (_, res, ctx) =>
    res(ctx.status(200), ctx.json(getAPIResponse(singleStoryDataMock)))
  )
);

const dispatchToBookmarks = jest.fn<Dispatch<BookmarksActions>, []>();

jest.mock('actions/bookmarks', () => ({ removeAllBookmarks: jest.fn() }));

beforeEach(() => {
  jest.clearAllMocks();
});

beforeAll(() => server.listen());

afterEach(() => {
  server.resetHandlers();

  localStorage.clear();
});

afterAll(() => server.close());

describe('tests to the Bookmarks view component', () => {
  test('should render a message when there are no bookmarks', () => {
    localStorage.setItem('bookmarks', JSON.stringify(initialBookmarksState));

    renderWithRouter(
      <BookmarksContext.Provider
        value={{ bookmarksState: initialBookmarksState, dispatchToBookmarks }}
      >
        <Route path={Paths.BookmarksPath} component={Bookmarks} />
      </BookmarksContext.Provider>,
      { route: '/bookmarks' }
    );

    const noBookmarksMessage = screen.getByText(
      /there are yet, but you can save any data that you like 😃/i
    );

    expect(noBookmarksMessage).toBeInTheDocument();
  });

  test('should render the headings of each type of bookmark and the remove all bookmarks button', () => {
    const initialBookmarksState = {
      characterBookmarks: [1017100],
      comicBookmarks: [40632],
      storiesBookmarks: [744]
    };

    localStorage.setItem('bookmarks', JSON.stringify(initialBookmarksState));

    renderWithRouter(
      <BookmarksContext.Provider
        value={{ bookmarksState: initialBookmarksState, dispatchToBookmarks }}
      >
        <Route path={Paths.BookmarksPath} component={Bookmarks} />
      </BookmarksContext.Provider>,
      { route: '/bookmarks' }
    );

    const charactersBookmarkHeading = screen.getByRole('heading', {
      name: /characters bookmarks/i
    });

    const comicsBookmarkHeading = screen.getByRole('heading', {
      name: /comics bookmarks/i
    });

    const storiesBookmarkHeading = screen.getByRole('heading', {
      name: /stories bookmarks/i
    });

    const removeAllBookmarksButton = screen.getByRole('button', {
      name: /remove all bookmarks/i
    });

    expect(charactersBookmarkHeading).toBeInTheDocument();

    expect(comicsBookmarkHeading).toBeInTheDocument();

    expect(storiesBookmarkHeading).toBeInTheDocument();

    expect(removeAllBookmarksButton).toBeInTheDocument();
  });

  test('should call the bookmarks action to remove all the bookmarks when the remove all bookmarks button is clicked', async () => {
    const initialBookmarksState = {
      characterBookmarks: [1017100],
      comicBookmarks: [40632],
      storiesBookmarks: [744]
    };

    localStorage.setItem('bookmarks', JSON.stringify(initialBookmarksState));

    renderWithRouter(
      <BookmarksContext.Provider
        value={{ bookmarksState: initialBookmarksState, dispatchToBookmarks }}
      >
        <Route path={Paths.BookmarksPath} component={Bookmarks} />
      </BookmarksContext.Provider>,
      { route: '/bookmarks' }
    );

    const removeAllBookmarksButton = screen.getByRole('button', {
      name: /remove all bookmarks/i
    });

    userEvent.click(removeAllBookmarksButton);

    expect(removeAllBookmarks).toBeCalled();
  });
});
