import { Dispatch } from 'react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { Route } from 'react-router-dom';
import {
  cleanup,
  screen,
  waitForElementToBeRemoved
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { BookmarksActions } from 'reducers/bookmarksReducer';

import { BookmarksContext } from 'contexts/BookmarksContext';
import { ComicsContextProvider } from 'contexts/ComicsContext';

import { getAPIResponse, getBaseUrl, renderWithRouter } from '__utils__';

import { addBookmark, removeBookmark } from 'actions/bookmarks';

import { initialBookmarksState, Paths } from 'components/App';

import { singleComicDataMock } from '__mocks__/comics';

import ComicDetails from 'views/Details/ComicDetails';

const server = setupServer(
  rest.get(getBaseUrl('comics/:id'), (_, res, ctx) =>
    res(ctx.status(200), ctx.json(getAPIResponse(singleComicDataMock)))
  )
);

const dispatchToBookmarks = jest.fn<Dispatch<BookmarksActions>, []>();

jest.mock('actions/bookmarks', () => ({
  addBookmark: jest.fn(),
  removeBookmark: jest.fn()
}));

beforeEach(() => {
  renderWithRouter(
    <BookmarksContext.Provider
      value={{ bookmarksState: initialBookmarksState, dispatchToBookmarks }}
    >
      <ComicsContextProvider>
        <Route path={Paths.ComicDetailsPath} component={ComicDetails} />
      </ComicsContextProvider>
    </BookmarksContext.Provider>,
    { route: '/details/comics/40632' }
  );

  localStorage.setItem('bookmarks', JSON.stringify(initialBookmarksState));

  jest.clearAllMocks();
});

beforeAll(() => server.listen());

afterEach(() => {
  server.resetHandlers();

  localStorage.clear();
});

afterAll(() => server.close());

describe('tests to the ComicDetails view component', () => {
  test('should render the loading indicator in the first render', () => {
    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    expect(loadingInfo).toBeInTheDocument();
  });

  test('should render all the proper elements', async () => {
    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    const comicHeading = screen.getByRole('heading', {
      name: /hulk \(2008\) #53/i
    });

    const comicImage = screen.getByRole('img', {
      name: /hulk \(2008\) #53 thumbnail/i
    });

    const infoContainers = screen.getAllByRole('contentinfo');

    const actionButtons = screen.getAllByRole('button');

    expect(comicHeading).toBeInTheDocument();

    expect(comicImage).toBeInTheDocument();

    expect(infoContainers).toHaveLength(3);

    expect(actionButtons).toHaveLength(2);
  });

  test('should be able to interact with the save button', async () => {
    const addBookmarkProps = {
      bookmarkListName: 'comicBookmarks',
      bookmarkId: 40632
    };

    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    userEvent.click(screen.getByRole('button', { name: /save/i }));

    expect(addBookmark).toBeCalledWith({ ...addBookmarkProps });
  });

  test('should be able to interact with the save button but to remove it from the bookmarks', async () => {
    cleanup();

    const removeBookmarkProps = {
      bookmarkId: 40632
    };

    const initialBookmarksState = {
      characterBookmarks: [],
      comicBookmarks: [40632],
      storiesBookmarks: []
    };

    renderWithRouter(
      <BookmarksContext.Provider
        value={{ bookmarksState: initialBookmarksState, dispatchToBookmarks }}
      >
        <ComicsContextProvider>
          <Route path={Paths.ComicDetailsPath} component={ComicDetails} />
        </ComicsContextProvider>
      </BookmarksContext.Provider>,
      { route: '/details/comics/40632' }
    );

    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    userEvent.click(
      screen.getByRole('button', { name: /remove from bookmarks/i })
    );

    expect(removeBookmark).toBeCalledWith({ ...removeBookmarkProps });
  });

  test('should be able to interact with the hide button and change the pathname', async () => {
    const removeBookmarkProps = {
      bookmarkId: 40632
    };

    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    userEvent.click(screen.getByRole('button', { name: /hide/i }));

    expect(removeBookmark).toBeCalledWith({ ...removeBookmarkProps });

    expect(window.location.pathname).toBe(Paths.ComicListPath);
  });
});
