import { Dispatch } from 'react';
import { rest } from 'msw';
import { Route } from 'react-router-dom';
import {
  cleanup,
  screen,
  waitForElementToBeRemoved
} from '@testing-library/react';
import { setupServer } from 'msw/node';
import userEvent from '@testing-library/user-event';

import { addBookmark, removeBookmark } from 'actions/bookmarks';

import { BookmarksActions } from 'reducers/bookmarksReducer';

import { BookmarksContext } from 'contexts/BookmarksContext';
import { CharactersContextProvider } from 'contexts/CharactersContext';

import { getAPIResponse, getBaseUrl, renderWithRouter } from '__utils__';

import { singleCharacterDataMock } from '__mocks__/characters';

import { initialBookmarksState, Paths } from 'components/App';

import CharacterDetails from 'views/Details/CharacterDetails';

const server = setupServer(
  rest.get(getBaseUrl('characters/:id'), (_, res, ctx) =>
    res(ctx.status(200), ctx.json(getAPIResponse(singleCharacterDataMock)))
  )
);

const dispatchToBookmarks = jest.fn<Dispatch<BookmarksActions>, []>();

jest.mock('actions/bookmarks', () => ({
  addBookmark: jest.fn(),
  removeBookmark: jest.fn()
}));

beforeEach(() => {
  renderWithRouter(
    <BookmarksContext.Provider
      value={{ bookmarksState: initialBookmarksState, dispatchToBookmarks }}
    >
      <CharactersContextProvider>
        <Route path={Paths.CharacterDetailsPath} component={CharacterDetails} />
      </CharactersContextProvider>
    </BookmarksContext.Provider>,
    { route: '/details/characters/1017100' }
  );

  localStorage.setItem('bookmarks', JSON.stringify(initialBookmarksState));

  jest.clearAllMocks();
});

beforeAll(() => server.listen());

afterEach(() => {
  server.resetHandlers();

  localStorage.clear();
});

afterAll(() => server.close());

describe('tests to the CharacterDetails view component', () => {
  test('should render the loading indicator in the first render', () => {
    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    expect(loadingInfo).toBeInTheDocument();
  });

  test('should render all the proper elements', async () => {
    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    const characterHeading = screen.getByRole('heading', {
      name: /a-bomb \(has\)/i
    });

    const characterImage = screen.getByRole('heading', {
      name: /a-bomb \(has\)/i
    });

    const infoContainers = screen.getAllByRole('contentinfo');

    const actionButtons = screen.getAllByRole('button');

    expect(characterHeading).toBeInTheDocument();

    expect(characterImage).toBeInTheDocument();

    expect(infoContainers).toHaveLength(3);

    expect(actionButtons).toHaveLength(2);
  });

  test('should be able to interact with the save button', async () => {
    const addBookmarkProps = {
      bookmarkListName: 'characterBookmarks',
      bookmarkId: 1017100
    };

    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    userEvent.click(screen.getByRole('button', { name: /save/i }));

    expect(addBookmark).toBeCalledWith({ ...addBookmarkProps });
  });

  test('should be able to interact with the save button but to remove it from the bookmarks', async () => {
    cleanup();

    const removeBookmarkProps = {
      bookmarkId: 1017100
    };

    const initialBookmarksState = {
      characterBookmarks: [1017100],
      comicBookmarks: [],
      storiesBookmarks: []
    };

    renderWithRouter(
      <BookmarksContext.Provider
        value={{ bookmarksState: initialBookmarksState, dispatchToBookmarks }}
      >
        <CharactersContextProvider>
          <Route
            path={Paths.CharacterDetailsPath}
            component={CharacterDetails}
          />
        </CharactersContextProvider>
      </BookmarksContext.Provider>,
      { route: '/details/characters/1017100' }
    );

    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    userEvent.click(
      screen.getByRole('button', { name: /remove from bookmarks/i })
    );

    expect(removeBookmark).toBeCalledWith({ ...removeBookmarkProps });
  });

  test('should be able to interact with the hide button and change the pathname', async () => {
    const removeBookmarkProps = {
      bookmarkId: 1017100
    };

    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    userEvent.click(screen.getByRole('button', { name: /hide/i }));

    expect(removeBookmark).toBeCalledWith({ ...removeBookmarkProps });

    expect(window.location.pathname).toBe(Paths.CharacterListPath);
  });
});
