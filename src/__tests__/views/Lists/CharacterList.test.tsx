import { rest } from 'msw';
import {
  screen,
  waitFor,
  waitForElementToBeRemoved
} from '@testing-library/react';
import { setupServer } from 'msw/node';
import selectEvent from 'react-select-event';
import userEvent from '@testing-library/user-event';

import { getAPIResponse, getBaseUrl, renderWithRouter } from '__utils__';

import { multipleCharactersDataMock } from '__mocks__/characters';
import { multipleComicsByIdAndName } from '__mocks__/comics';
import { multipleStoriesByIdAndName } from '__mocks__/stories';

import { CharactersContextProvider } from 'contexts/CharactersContext';

import CharacterList from 'views/Lists/CharacterList';
import { Route } from 'react-router-dom';
import { Paths } from 'components/App';

const server = setupServer(
  rest.get(getBaseUrl('characters', 'offset=0'), (_, res, ctx) =>
    res(ctx.status(200), ctx.json(getAPIResponse(multipleCharactersDataMock)))
  ),
  rest.get(getBaseUrl('comics', 'limit=100'), (_, res, ctx) =>
    res(ctx.status(200), ctx.json(getAPIResponse(multipleComicsByIdAndName)))
  ),
  rest.get(getBaseUrl('stories', 'limit=100'), (_, res, ctx) =>
    res(ctx.status(200), ctx.json(getAPIResponse(multipleStoriesByIdAndName)))
  )
);

beforeEach(() => {
  renderWithRouter(
    <CharactersContextProvider>
      <Route path={Paths.CharacterListPath} component={CharacterList} />
    </CharactersContextProvider>,
    { route: '/list/characters' }
  );
});

beforeAll(() => server.listen());

afterEach(() => {
  server.resetHandlers();

  jest.clearAllTimers();
  jest.clearAllMocks();
});

afterAll(() => server.close());

describe('tests to the CharacterList view component', () => {
  test('should render the loading indicator in the first render', () => {
    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    expect(loadingInfo).toBeInTheDocument();
  });

  test('should render the list of cards', async () => {
    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    const cards = screen.getAllByRole('link');

    expect(cards).toHaveLength(20);
  });

  test('should be able to interact with the select by comics', async () => {
    // -------------------------------------------------------------------------
    // Enviroment Variables

    const filterOptions = {
      value: 'comics',
      label: 'Comics'
    };

    const charactersComics = {
      value: '23561',
      label: 'Holiday Special (1969) #1'
    };

    // -------------------------------------------------------------------------

    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    const filterForm = screen.getByTestId(/filterForm/i);
    const filterOptionsSelect = screen.getByLabelText(/filter options/i);

    await selectEvent.select(filterOptionsSelect, [filterOptions.label]);

    expect(filterForm).toHaveFormValues({ filterOptions: filterOptions.value });

    const filtersSelectionForm = screen.getByTestId(/filtersSelectionForm/i);
    const filterByComicsSelect = screen.getByLabelText(/by comics/i);

    expect(filtersSelectionForm).toBeInTheDocument();

    await selectEvent.select(filterByComicsSelect, [charactersComics.label]);

    expect(filtersSelectionForm).toHaveFormValues({
      charactersComics: charactersComics.value
    });

    await selectEvent.clearAll(filterOptionsSelect);

    expect(filtersSelectionForm).not.toBeInTheDocument();
  });

  test('should be able to interact with the select by stories', async () => {
    // -------------------------------------------------------------------------
    // Enviroment Variables

    const filterOptions = {
      value: 'stories',
      label: 'Stories'
    };

    const charactersStories = {
      value: '8',
      label:
        'In the wake of September 11th, the world watched as firefighters, police officers and EMT workers selflessly risked their lives'
    };

    // -------------------------------------------------------------------------

    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    const filterForm = screen.getByTestId(/filterForm/i);
    const filterOptionsSelect = screen.getByLabelText(/filter options/i);

    await selectEvent.select(filterOptionsSelect, [filterOptions.label]);

    expect(filterForm).toHaveFormValues({ filterOptions: filterOptions.value });

    const filtersSelectionForm = screen.getByTestId(/filtersSelectionForm/i);
    const filterByStoriesSelect = screen.getByLabelText(/by stories/i);

    expect(filtersSelectionForm).toBeInTheDocument();

    await selectEvent.select(filterByStoriesSelect, [charactersStories.label]);

    expect(filtersSelectionForm).toHaveFormValues({
      charactersStories: charactersStories.value
    });

    await selectEvent.clearAll(filterOptionsSelect);

    expect(filtersSelectionForm).not.toBeInTheDocument();
  });

  test('should be able to interact with the search by title', async () => {
    // -------------------------------------------------------------------------
    // Enviroment Variables

    const filterOptions = {
      label: 'Name',
      value: 'nameStartsWith'
    };

    const charactersName = 'Spider';

    // -------------------------------------------------------------------------

    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    const filterForm = screen.getByTestId(/filterForm/i);
    const filterOptionsSelect = screen.getByLabelText(/filter options/i);

    await selectEvent.select(filterOptionsSelect, [filterOptions.label]);

    expect(filterForm).toHaveFormValues({ filterOptions: filterOptions.value });

    const filtersSelectionForm = screen.getByTestId(/filtersSelectionForm/i);
    const filterByNameSearch = screen.getByLabelText(/by name/i);

    userEvent.type(filterByNameSearch, charactersName);

    await waitFor(() =>
      expect(filtersSelectionForm).toHaveFormValues({ charactersName })
    );

    await selectEvent.clearAll(filterForm);

    expect(filtersSelectionForm).not.toBeInTheDocument();
  });

  test('should be able to interact with a card and change the pathname', async () => {
    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    const card = screen.getAllByRole('link')[0];

    userEvent.click(card);

    expect(window.location.pathname).toBe('/details/characters/1017100');
  });
});
