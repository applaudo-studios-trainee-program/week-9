import { rest } from 'msw';
import { Route } from 'react-router-dom';
import { setupServer } from 'msw/node';
import {
  fireEvent,
  screen,
  waitForElementToBeRemoved
} from '@testing-library/react';
import selectEvent from 'react-select-event';
import userEvent from '@testing-library/user-event';

import { getAPIResponse, getBaseUrl, renderWithRouter } from '__utils__';

import { multipleStoriesDataMock } from '__mocks__/stories';

import { Paths } from 'components/App';

import { StoriesContextProvider } from 'contexts/StoriesContext';

import StoryList from 'views/Lists/StoryList';

const server = setupServer(
  rest.get(getBaseUrl('stories', 'offset=0'), (_, res, ctx) =>
    res(ctx.status(200), ctx.json(getAPIResponse(multipleStoriesDataMock)))
  )
);

beforeEach(() => {
  renderWithRouter(
    <StoriesContextProvider>
      <Route path={Paths.StoryListPath} component={StoryList} />
    </StoriesContextProvider>,
    { route: '/list/stories' }
  );
});

beforeAll(() => server.listen());

afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('tests to the StoryList view component', () => {
  test('should render the loading indicator in the first render', () => {
    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    expect(loadingInfo).toBeInTheDocument();
  });

  test('should render the list of cards', async () => {
    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    const cards = screen.getAllByRole('link');

    expect(cards).toHaveLength(20);
  });

  test('should be able to interact with the date picker', async () => {
    // -------------------------------------------------------------------------
    // Enviroment Variables

    const filterOptions = {
      value: 'modifiedSince',
      label: 'Last Modified'
    };

    const storiesDate = '2021-01-06';

    // -------------------------------------------------------------------------

    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    const filterForm = screen.getByTestId(/filterForm/i);
    const filterOptionsSelect = screen.getByLabelText(/filter options/i);

    await selectEvent.select(filterOptionsSelect, [filterOptions.label]);

    expect(filterForm).toHaveFormValues({ filterOptions: filterOptions.value });

    const filtersSelectionForm = screen.getByTestId(/filtersSelectionForm/i);
    const filterByDateDatePicker = screen.getByLabelText(/by date/i);

    expect(filtersSelectionForm).toBeInTheDocument();

    fireEvent.change(filterByDateDatePicker, {
      target: { value: storiesDate }
    });

    expect(filtersSelectionForm).toHaveFormValues({ storiesDate });

    await selectEvent.clearAll(filterOptionsSelect);

    expect(filtersSelectionForm).not.toBeInTheDocument();
  });

  test('should be able to interact with a card and change the pathname', async () => {
    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    const card = screen.getAllByRole('link')[0];

    userEvent.click(card);

    expect(window.location.pathname).toBe('/details/stories/7');
  });
});
