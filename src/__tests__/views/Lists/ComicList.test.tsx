import { rest } from 'msw';
import { screen, waitForElementToBeRemoved } from '@testing-library/react';
import { setupServer } from 'msw/node';
import selectEvent from 'react-select-event';
import userEvent from '@testing-library/user-event';

import { getBaseUrl, getAPIResponse, renderWithRouter } from '__utils__';

import { multipleComicsDataMock } from '__mocks__/comics';

import { ComicsContextProvider } from 'contexts/ComicsContext';

import ComicList from 'views/Lists/ComicList';
import { Route } from 'react-router-dom';
import { Paths } from 'components/App';

const server = setupServer(
  rest.get(getBaseUrl('comics', 'offset=0'), (_, res, ctx) =>
    res(ctx.status(200), ctx.json(getAPIResponse(multipleComicsDataMock)))
  )
);

beforeEach(() => {
  renderWithRouter(
    <ComicsContextProvider>
      <Route path={Paths.ComicListPath} component={ComicList} />
    </ComicsContextProvider>,
    { route: '/list/comics' }
  );
});

beforeAll(() => server.listen());

afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('tests in the ComicList view component', () => {
  test('should render the loading indicator in the first render', () => {
    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    expect(loadingInfo).toBeInTheDocument();
  });

  test('should render the list of cards', async () => {
    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    const cards = screen.getAllByRole('link');

    expect(cards).toHaveLength(20);
  });

  test('should be able to interact with the select by format', async () => {
    // -------------------------------------------------------------------------
    // Enviroment Variables

    const filterOptions = {
      value: 'format',
      label: 'Format'
    };

    const comicsFormats = {
      value: 'comic',
      label: 'Comic'
    };

    // -------------------------------------------------------------------------

    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    const filterForm = screen.getByTestId(/filterForm/i);
    const filterOptionsSelect = screen.getByLabelText(/filter options/i);

    await selectEvent.select(filterOptionsSelect, [filterOptions.label]);

    expect(filterForm).toHaveFormValues({ filterOptions: filterOptions.value });

    const filtersSelectionForm = screen.getByTestId(/filtersSelectionForm/i);
    const filterByFormatSelect = screen.getByLabelText(/by format/i);

    expect(filtersSelectionForm).toBeInTheDocument();

    await selectEvent.select(filterByFormatSelect, [comicsFormats.label]);

    expect(filtersSelectionForm).toHaveFormValues({
      comicsFormats: comicsFormats.value
    });

    await selectEvent.clearAll(filterOptionsSelect);

    expect(filtersSelectionForm).not.toBeInTheDocument();
  });

  test('should be able to interact with the search by title', async () => {
    // -------------------------------------------------------------------------
    // Enviroment Variables

    const filterOptions = {
      label: 'Title',
      value: 'titleStartsWith'
    };

    const comicsTitle = 'Spider';

    // -------------------------------------------------------------------------

    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    const filterForm = screen.getByTestId(/filterForm/i);
    const filterOptionsSelect = screen.getByLabelText(/filter options/i);

    await selectEvent.select(filterOptionsSelect, [filterOptions.label]);

    expect(filterForm).toHaveFormValues({ filterOptions: filterOptions.value });

    const filtersSelectionForm = screen.getByTestId(/filtersSelectionForm/i);
    const filterByTitleSelect = screen.getByLabelText(/by title/i);

    expect(filtersSelectionForm).toBeInTheDocument();

    userEvent.type(filterByTitleSelect, comicsTitle);

    expect(filtersSelectionForm).toHaveFormValues({ comicsTitle });

    await selectEvent.clearAll(filterOptionsSelect);

    expect(filtersSelectionForm).not.toBeInTheDocument();
  });

  test('should be able to interact with a card and change the pathname', async () => {
    const loadingInfo = screen.getByRole('heading', { name: /loading data/i });

    await waitForElementToBeRemoved(loadingInfo);

    const card = screen.getAllByRole('link')[0];

    userEvent.click(card);

    expect(window.location.pathname).toBe('/details/comics/1689');
  });
});
