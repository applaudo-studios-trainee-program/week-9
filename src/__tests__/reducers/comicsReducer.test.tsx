import {
  ComicsFilters,
  comicsReducer,
  ComicsState
} from 'reducers/comicsReducer';

describe('tests to the charactersReducer', () => {
  test('should return the initial state if a invalid action is passed', () => {
    const initialState: ComicsState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const state = comicsReducer(initialState, {
      type: 'SET_NEW_DATA'
    } as any);

    expect(state).toEqual(initialState);
  });

  test("should set data when it's passed to the reducer", () => {
    const initialState: ComicsState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const data = [
      {
        id: 1689,
        title:
          'Official Handbook of the Marvel Universe (2004) #10 (MARVEL KNIGHTS)',
        thumbnail:
          'http://i.annihil.us/u/prod/marvel/i/mg/9/30/4bc64df4105b9.jpg'
      }
    ];

    const state = comicsReducer(initialState, {
      type: 'SET_DATA',
      payload: { data, offset: 0, total: 20 }
    });

    expect(state).toEqual({
      ...initialState,
      total: 20,
      data
    });
  });

  test('should remove the data that is sent to the reducer if the element id is in the removedElements list', () => {
    const initialState: ComicsState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: [1689]
    };

    const data = [
      {
        id: 1689,
        title:
          'Official Handbook of the Marvel Universe (2004) #10 (MARVEL KNIGHTS)',
        thumbnail:
          'http://i.annihil.us/u/prod/marvel/i/mg/9/30/4bc64df4105b9.jpg'
      }
    ];

    const state = comicsReducer(initialState, {
      type: 'SET_DATA',
      payload: { data, offset: 0, total: 20 }
    });

    expect(state).toEqual({ ...initialState, total: 20, data: [] });
  });

  test('should set the filterData values', () => {
    const initialState: ComicsState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const filterData: ComicsFilters = {
      filterName: 'format',
      filterValue: 'Spider'
    };

    const state = comicsReducer(initialState, {
      type: 'SET_FILTER_DATA',
      payload: { filterData }
    });

    expect(state).toEqual({ ...initialState, filterData });
  });

  test('should remove the filterData values', () => {
    const initialState: ComicsState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: { filterName: 'format', filterValue: 'Spider' },
      removedElements: []
    };

    const state = comicsReducer(initialState, { type: 'REMOVE_FILTER' });

    expect(state).toEqual({ ...initialState, filterData: null });
  });

  test('should return a state with the removed element id in an array', () => {
    const initialState: ComicsState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const removedElementId = 595;

    const state = comicsReducer(initialState, {
      type: 'ADD_ENTRY_TO_HIDE',
      payload: { comicId: removedElementId }
    });

    expect(state).toEqual({
      ...initialState,
      removedElements: [removedElementId]
    });
  });
});
