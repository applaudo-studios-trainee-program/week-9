import {
  bookmarksReducer,
  BookmarksState,
  BookmarksTypes
} from 'reducers/bookmarksReducer';

describe('tests to the bookmarksReducer', () => {
  test('should return the initialState if an invalid action is passed', () => {
    const initialBookmarksState: BookmarksState = {
      characterBookmarks: [],
      comicBookmarks: [],
      storiesBookmarks: []
    };

    const state = bookmarksReducer(initialBookmarksState, {
      type: 'ADD_NEW_BOOKMARK'
    } as any);

    expect(state).toEqual(initialBookmarksState);
  });

  test('should set the bookmark to the correct list', () => {
    const initialBookmarksState: BookmarksState = {
      characterBookmarks: [],
      comicBookmarks: [],
      storiesBookmarks: []
    };

    const bookmarkData: {
      bookmarkListName: BookmarksTypes;
      bookmarkId: number;
    } = {
      bookmarkId: 59845,
      bookmarkListName: 'characterBookmarks'
    };

    const state = bookmarksReducer(initialBookmarksState, {
      type: 'ADD_BOOKMARK',
      payload: bookmarkData
    });

    expect(state).toEqual({
      ...initialBookmarksState,
      characterBookmarks: [bookmarkData.bookmarkId]
    });
  });

  test('should remove the correct bookmark according to an id', () => {
    const initialBookmarksState: BookmarksState = {
      characterBookmarks: [658],
      comicBookmarks: [598],
      storiesBookmarks: [594]
    };

    const bookmarkData: { bookmarkId: number } = {
      bookmarkId: 594
    };

    const state = bookmarksReducer(initialBookmarksState, {
      type: 'REMOVE_BOOKMARK',
      payload: bookmarkData
    });

    expect(state).toEqual({
      ...initialBookmarksState,
      storiesBookmarks: []
    });
  });

  test('should remove all the bookmarks', () => {
    const initialBookmarksState: BookmarksState = {
      characterBookmarks: [658],
      comicBookmarks: [598],
      storiesBookmarks: [594]
    };

    const state = bookmarksReducer(initialBookmarksState, {
      type: 'REMOVE_ALL_BOOKMARKS'
    });

    expect(state).toEqual({
      characterBookmarks: [],
      comicBookmarks: [],
      storiesBookmarks: []
    });
  });
});
