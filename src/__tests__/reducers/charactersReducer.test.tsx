import {
  CharactersFilters,
  charactersReducer,
  CharactersState
} from 'reducers/charactersReducer';

describe('tests to the charactersReducer', () => {
  test('should return the initial state if a invalid action is passed', () => {
    const initialState: CharactersState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const state = charactersReducer(initialState, {
      type: 'SET_NEW_DATA'
    } as any);

    expect(state).toEqual(initialState);
  });

  test("should set data when it's passed to the reducer", () => {
    const initialState: CharactersState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const data = [
      {
        id: 1017100,
        name: 'A-Bomb (HAS)',
        thumbnail:
          'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16.jpg'
      }
    ];

    const state = charactersReducer(initialState, {
      type: 'SET_DATA',
      payload: { data, offset: 0, total: 20 }
    });

    expect(state).toEqual({
      ...initialState,
      total: 20,
      data
    });
  });

  test('should remove the data that is sent to the reducer if the element id is in the removedElements list', () => {
    const initialState: CharactersState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: [1017100]
    };

    const data = [
      {
        id: 1017100,
        name: 'A-Bomb (HAS)',
        thumbnail:
          'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16.jpg'
      }
    ];

    const state = charactersReducer(initialState, {
      type: 'SET_DATA',
      payload: { data, offset: 0, total: 20 }
    });

    expect(state).toEqual({ ...initialState, total: 20, data: [] });
  });

  test('should set the filterData values', () => {
    const initialState: CharactersState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const filterData: CharactersFilters = {
      filterName: 'comics',
      filterValue: '55'
    };

    const state = charactersReducer(initialState, {
      type: 'SET_FILTER_DATA',
      payload: { filterData }
    });

    expect(state).toEqual({ ...initialState, filterData });
  });

  test('should remove the filterData values', () => {
    const initialState: CharactersState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: { filterName: 'comics', filterValue: '595' },
      removedElements: []
    };

    const state = charactersReducer(initialState, { type: 'REMOVE_FILTER' });

    expect(state).toEqual({ ...initialState, filterData: null });
  });

  test('should return a state with the removed element id in an array', () => {
    const initialState: CharactersState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const removedElementId = 595;

    const state = charactersReducer(initialState, {
      type: 'ADD_ENTRY_TO_HIDE',
      payload: { characterId: removedElementId }
    });

    expect(state).toEqual({
      ...initialState,
      removedElements: [removedElementId]
    });
  });
});
