import {
  StoriesFilters,
  storiesReducer,
  StoriesState
} from 'reducers/storiesReducer';

describe('tests to the charactersReducer', () => {
  test('should return the initial state if a invalid action is passed', () => {
    const initialState: StoriesState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const state = storiesReducer(initialState, {
      type: 'SET_NEW_DATA'
    } as any);

    expect(state).toEqual(initialState);
  });

  test("should set data when it's passed to the reducer", () => {
    const initialState: StoriesState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const data = [
      {
        id: 7,
        title:
          'Investigating the murder of a teenage girl, Cage suddenly learns that a three-way gang war is under way for control of the turf',
        description: ''
      }
    ];

    const state = storiesReducer(initialState, {
      type: 'SET_DATA',
      payload: { data, offset: 0, total: 20 }
    });

    expect(state).toEqual({
      ...initialState,
      total: 20,
      data
    });
  });

  test('should remove the data that is sent to the reducer if the element id is in the removedElements list', () => {
    const initialState: StoriesState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: [1689]
    };

    const data = [
      {
        id: 1689,
        title:
          'Official Handbook of the Marvel Universe (2004) #10 (MARVEL KNIGHTS)',
        thumbnail:
          'http://i.annihil.us/u/prod/marvel/i/mg/9/30/4bc64df4105b9.jpg'
      }
    ];

    const state = storiesReducer(initialState, {
      type: 'SET_DATA',
      payload: { data, offset: 0, total: 20 }
    });

    expect(state).toEqual({ ...initialState, total: 20, data: [] });
  });

  test('should set the filterData values', () => {
    const initialState: StoriesState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const filterData: StoriesFilters = {
      filterName: 'modifiedSince',
      filterValue: '2021-12-03'
    };

    const state = storiesReducer(initialState, {
      type: 'SET_FILTER_DATA',
      payload: { filterData }
    });

    expect(state).toEqual({ ...initialState, filterData });
  });

  test('should remove the filterData values', () => {
    const initialState: StoriesState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: { filterName: 'modifiedSince', filterValue: '2021-12-03' },
      removedElements: []
    };

    const state = storiesReducer(initialState, { type: 'REMOVE_FILTER' });

    expect(state).toEqual({ ...initialState, filterData: null });
  });

  test('should return a state with the removed element id in an array', () => {
    const initialState: StoriesState = {
      data: [],
      offset: 0,
      total: 0,
      filterData: null,
      removedElements: []
    };

    const removedElementId = 595;

    const state = storiesReducer(initialState, {
      type: 'ADD_ENTRY_TO_HIDE',
      payload: { storyId: removedElementId }
    });

    expect(state).toEqual({
      ...initialState,
      removedElements: [removedElementId]
    });
  });
});
