import styled from '@emotion/styled';

const StyledHeading = styled.h1`
  color: var(--secondary-white-color);
  letter-spacing: 0.02em;
`;

const LoadingData = () => <StyledHeading>Loading Data...</StyledHeading>;

export default LoadingData;
