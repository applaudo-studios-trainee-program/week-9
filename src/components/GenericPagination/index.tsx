import { Link } from 'react-router-dom';

import { memo } from 'react';

import Pagination from 'rc-pagination';

import { Characters } from 'interfaces/characters';
import { Comics } from 'interfaces/comics';
import { Stories } from 'interfaces/stories';

import Card from 'components/Card';
import Grid from 'components/Grid';

import 'rc-pagination/assets/index.css';

type Props<T extends Characters | Comics | Stories> = {
  data: T[];
  limit: number;
  offset: number;
  total: number;
  type: 'comics' | 'characters' | 'stories';
  onPageChange: (page: number, pageSize: number) => void;
};

const GenericPagination = <DataToRender extends Characters | Comics | Stories>({
  data,
  limit,
  offset,
  total,
  type,
  onPageChange
}: Props<DataToRender>) => (
  <>
    <Grid gap={3}>
      {data.length ? (
        data.map(moreData => (
          <Link to={`/details/${type}/${moreData.id}`} key={moreData.id}>
            <Card {...moreData} />
          </Link>
        ))
      ) : (
        <span style={{ gridColumn: '1 / span 2' }}>
          We <strong>could not</strong> find any results that could match your
          search news 😔
        </span>
      )}
    </Grid>

    {total > limit ? (
      <Pagination
        current={offset}
        pageSize={limit}
        style={{ display: 'flex', justifyContent: 'center' }}
        total={total}
        showLessItems
        onChange={onPageChange}
      />
    ) : null}
  </>
);

export default memo(GenericPagination);
