import { memo } from 'react';
import styled from '@emotion/styled';

type LayoutProps = { maxWidthInREM?: number };

const Container = styled.section<LayoutProps>`
  margin: 0 auto;

  max-width: ${({ maxWidthInREM }) => maxWidthInREM || 120}rem;

  padding: 2rem;

  @media (min-width: 340px) {
    padding: 3rem;
  }

  ${({ maxWidthInREM }) =>
    maxWidthInREM ? null : `@media (min-width: 1500px) { max-width: 150rem }`}
`;

export default memo(Container);
