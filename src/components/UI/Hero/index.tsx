import { memo } from 'react';

import styles from './Hero.module.css';

const Hero = () => (
  <section className={styles.container}>
    <h1>Marvel DOCS</h1>
    <p>Your realiable source of Marvel</p>
  </section>
);

export default memo(Hero);
