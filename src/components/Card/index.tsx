import { memo } from 'react';
import styled from '@emotion/styled';

import Column from 'components/Layouts/Alignment/Column';

const StyledCardContainer = styled.div`
  height: 25rem;

  position: relative;

  line-height: 0;
`;

const StyledCardImage = styled.img`
  border-radius: var(--border-radius);

  object-fit: cover;
`;

const StyledCardTitle = styled.strong`
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;

  display: -webkit-box;

  letter-spacing: 0.01em;

  line-height: 1.5;

  overflow: hidden;

  text-overflow: ellipsis;
`;

const StyledCardDescription = styled.small`
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;

  display: -webkit-box;

  letter-spacing: 0.01em;

  line-height: 1.5;

  overflow: hidden;

  text-overflow: ellipsis;
`;

const StyledCardData = styled.div`
  background: linear-gradient(
    360deg,
    rgba(0, 0, 0, 0.6) 0%,
    rgba(28, 27, 27, 0.5) 50%,
    rgba(255, 255, 255, 0) 100%
  );

  border-bottom-left-radius: var(--border-radius);
  border-bottom-right-radius: var(--border-radius);

  bottom: 0;

  color: var(--primary-white-color);

  padding: 2rem;

  position: absolute;

  width: 100%;
`;

type Props = {
  description?: string;
  name?: string;
  thumbnail?: string;
  title?: string;
};

const Card = ({ description, name, thumbnail, title }: Props) => (
  <StyledCardContainer>
    <StyledCardImage
      alt={`${name || title} Thumbnail`}
      loading="lazy"
      src={
        thumbnail ||
        'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available/landscape_incredible.jpg'
      }
      title={`${name || title} Thumbnail`}
    />

    <StyledCardData>
      <Column gap={1}>
        <StyledCardTitle>{name || title}</StyledCardTitle>

        {description ? (
          <StyledCardDescription>{description}</StyledCardDescription>
        ) : null}
      </Column>
    </StyledCardData>
  </StyledCardContainer>
);

export default memo(Card);
