import { memo } from 'react';
import styled from '@emotion/styled';

type LayoutProps = { gap?: number; flex?: boolean };

const Column = styled.div<LayoutProps>`
  display: flex;

  ${({ flex }) => (flex ? 'flex: 1;' : null)}
  flex-direction: column;

  ${({ gap }) => (gap ? `gap: ${gap}rem;` : null)}
`;

export default memo(Column);
