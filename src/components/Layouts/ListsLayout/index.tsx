import { lazy, memo } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { Paths } from 'components/App';

import Container from 'components/Container';

const CharacterList = lazy(
  () => import(/* webpackPrefetch: true */ 'views/Lists/CharacterList')
);

const ComicList = lazy(
  () => import(/* webpackPrefetch: true */ 'views/Lists/ComicList')
);

const StoryList = lazy(
  () => import(/* webpackPrefetch: true */ 'views/Lists/StoryList')
);

const ListsLayout = () => (
  <Container>
    <Switch>
      <Route exact path={Paths.CharacterListPath} component={CharacterList} />
      <Route exact path={Paths.ComicListPath} component={ComicList} />
      <Route exact path={Paths.StoryListPath} component={StoryList} />

      <Redirect to="/" />
    </Switch>
  </Container>
);

export default memo(ListsLayout);
