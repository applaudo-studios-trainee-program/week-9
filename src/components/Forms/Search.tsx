import { ChangeEvent, memo } from 'react';
import styled from '@emotion/styled';

type Props = {
  inputId?: string;
  name?: string;
  placeholder?: string;
  value?: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
};

const StyledSearch = styled.input`
  border: none;
  border-radius: var(--border-radius);

  height: 3.8rem;

  outline: none;

  padding: 0.5rem 2rem;

  &:focus {
    box-shadow: 0 0 0 0.2rem #2684ff;
  }
`;

const Search = ({ inputId, name, placeholder, value, onChange }: Props) => (
  <StyledSearch
    id={inputId}
    name={name}
    type="text"
    value={value}
    placeholder={placeholder}
    onChange={onChange}
  />
);

export default memo(Search);
