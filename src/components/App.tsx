import { Suspense, lazy, useReducer } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';

import { BookmarksContext } from 'contexts/BookmarksContext';
import { CharactersContextProvider } from 'contexts/CharactersContext';
import { ComicsContextProvider } from 'contexts/ComicsContext';
import { StoriesContextProvider } from 'contexts/StoriesContext';

import { bookmarksReducer, BookmarksState } from 'reducers/bookmarksReducer';

import Navbar from 'components/UI/Navbar';

import Spinner from 'assets/icons/Spinner';

const Home = lazy(() => import(/* webpackPrefetch: true */ 'views/Home'));

const Bookmarks = lazy(
  () => import(/* webpackPrefetch: true */ 'views/Bookmarks')
);

const ListsLayout = lazy(
  () => import(/* webpackPrefetch: true */ 'components/Layouts/ListsLayout')
);

const DetailsLayout = lazy(
  () => import(/* webpackPrefetch: true */ 'components/Layouts/DetailsLayout')
);

export enum Paths {
  HomePath = '/',
  BookmarksPath = '/bookmarks',

  ListsPath = '/list/:path',
  CharacterListPath = '/list/characters',
  ComicListPath = '/list/comics',
  StoryListPath = '/list/stories',

  DetailsPath = '/details/:path',
  CharacterDetailsPath = '/details/characters/:id',
  ComicDetailsPath = '/details/comics/:id',
  StoryDetailsPath = '/details/stories/:id'
}

export const initialBookmarksState: BookmarksState = {
  characterBookmarks: [],
  comicBookmarks: [],
  storiesBookmarks: []
};

const initFn = () => {
  if (JSON.parse(localStorage.getItem('bookmarks') || 'null')) {
    return JSON.parse(localStorage.getItem('bookmarks') || 'null');
  }

  localStorage.setItem('bookmarks', JSON.stringify(initialBookmarksState));

  return initialBookmarksState;
};

const App = () => {
  const [bookmarksState, dispatchToBookmarks] = useReducer(
    bookmarksReducer,
    {},
    initFn
  );

  return (
    <BookmarksContext.Provider value={{ bookmarksState, dispatchToBookmarks }}>
      <CharactersContextProvider>
        <ComicsContextProvider>
          <StoriesContextProvider>
            <Router>
              <Navbar />

              <main
                style={{
                  backgroundColor: 'var(--secondary-dark-color)',
                  minHeight: '100vh'
                }}
              >
                <Suspense fallback={<Spinner />}>
                  <Switch>
                    <Route exact path={Paths.HomePath} component={Home} />
                    <Route
                      exact
                      path={Paths.BookmarksPath}
                      component={Bookmarks}
                    />
                    <Route path={Paths.ListsPath} component={ListsLayout} />
                    <Route path={Paths.DetailsPath} component={DetailsLayout} />

                    <Redirect to="/" />
                  </Switch>
                </Suspense>
              </main>
            </Router>
          </StoriesContextProvider>
        </ComicsContextProvider>
      </CharactersContextProvider>
    </BookmarksContext.Provider>
  );
};

export default App;
