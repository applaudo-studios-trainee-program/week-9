import { ComicsItem, StoriesItem } from 'interfaces/characters';

type Props<T> = { dataToList: T[] };

const ItemList = <Data extends ComicsItem | StoriesItem>({
  dataToList
}: Props<Data>) => (
  <span>
    {dataToList.map((data, index) => {
      if (index === 0) return data.name;

      return `, ${data.name}`;
    })}
  </span>
);

export default ItemList;
