import { dataFetcher } from 'services/client';

import { ComicsFilters } from 'reducers/comicsReducer';

import { ComicsResponse, Comics, Comic } from 'interfaces/comics';

type GetComicsProps = { offset: number; filter?: Required<ComicsFilters> };

export const getComics = async ({ offset, filter }: GetComicsProps) => {
  const urlParams = filter
    ? `offset=${offset}&${filter.filterName}=${encodeURI(filter.filterValue)}`
    : `offset=${offset}`;

  const response = await dataFetcher({ endpoint: 'comics', urlParams });

  const { data }: ComicsResponse = await response.json();

  const results: Comics[] = data.results.map(({ id, title, thumbnail }) => {
    const { extension, path } = thumbnail;

    return {
      id,
      title,
      thumbnail: `${path}/landscape_incredible.${extension}`
    };
  });

  return {
    data: results,
    limit: data.limit,
    offset: data.offset,
    total: data.total
  };
};

export const getComicsIdAndName = async () => {
  const urlParams = 'limit=100';

  const response = await dataFetcher({ endpoint: 'comics', urlParams });
  const { data }: ComicsResponse = await response.json();

  return data.results.map(({ id, title }) => ({
    value: id.toString(),
    label: title
  }));
};

type GetComicByIdProps = { id: string };

export const getComicById = async ({ id }: GetComicByIdProps) => {
  const response = await dataFetcher({ endpoint: `comics/${id}` });

  const { data }: ComicsResponse = await response.json();

  const results: Comic = data.results.map(
    ({ id, title, description, thumbnail, characters, creators }) => {
      const { extension, path } = thumbnail;

      const { items: characterItems } = characters;
      const { items: creatorItems } = creators;

      return {
        characterItems,
        creatorItems,
        description,
        id,
        thumbnail: `${path}/standard_fantastic.${extension}`,
        title
      };
    }
  )[0];

  return results;
};
