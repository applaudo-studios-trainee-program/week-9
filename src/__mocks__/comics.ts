export const singleComicDataMock = [
  {
    id: 40632,
    title: 'Hulk (2008) #53',
    description:
      'The Mayan Gods are here! Guest starring Alpha Flight, Machine Man, She-Hulks, A-Bomb!',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/2/00/5ba3bfcc55f5a',
      extension: 'jpg'
    },
    creators: {
      available: 5,
      collectionURI:
        'http://gateway.marvel.com/v1/public/comics/40632/creators',
      items: [
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/creators/11482',
          name: 'Jesus Aburtov',
          role: 'colorist'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/creators/10172',
          name: 'Vc Clayton Cowles',
          role: 'letterer'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/creators/3646',
          name: 'Dale Eaglesham',
          role: 'penciller (cover)'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/creators/4600',
          name: 'Mark Paniccia',
          role: 'editor'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/creators/824',
          name: 'Jeff Parker',
          role: 'writer'
        }
      ],
      returned: 5
    },
    characters: {
      available: 12,
      collectionURI:
        'http://gateway.marvel.com/v1/public/comics/40632/characters',
      items: [
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1017100',
          name: 'A-Bomb (HAS)'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1010370',
          name: 'Alpha Flight'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009163',
          name: 'Aurora'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009330',
          name: 'Guardian'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1010805',
          name: 'Machine Man'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009513',
          name: 'Puck'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1011360',
          name: 'Red Hulk'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009560',
          name: 'Sasquatch (Walter Langkowski)'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009583',
          name: 'She-Hulk (Jennifer Walters)'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1011392',
          name: 'She-Hulk (Lyra)'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009606',
          name: 'Snowbird'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1011003',
          name: 'Thaddeus Ross'
        }
      ],
      returned: 12
    }
  }
];

export const multipleComicsDataMock = [
  {
    id: 1689,
    title:
      'Official Handbook of the Marvel Universe (2004) #10 (MARVEL KNIGHTS)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/30/4bc64df4105b9',
      extension: 'jpg'
    }
  },
  {
    id: 82967,
    title: 'Marvel Previews (2017)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
      extension: 'jpg'
    }
  },
  {
    id: 82965,
    title: 'Marvel Previews (2017)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
      extension: 'jpg'
    }
  },
  {
    id: 82970,
    title: 'Marvel Previews (2017)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/80/5e3d7536c8ada',
      extension: 'jpg'
    }
  },
  {
    id: 1994,
    title: 'Official Handbook of the Marvel Universe (2004) #13 (TEAMS)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/f/20/4bc63a47b8dcb',
      extension: 'jpg'
    }
  },
  {
    id: 1886,
    title: 'Official Handbook of the Marvel Universe (2004) #12 (SPIDER-MAN)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/4bc64020a4ccc',
      extension: 'jpg'
    }
  },
  {
    id: 428,
    title: 'Ant-Man (2003) #4',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/4/20/4bc697c680890',
      extension: 'jpg'
    }
  },
  {
    id: 384,
    title: 'Gun Theory (2003) #3',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/60/4bc69f11baf75',
      extension: 'jpg'
    }
  },
  {
    id: 1308,
    title: 'Marvel Age Spider-Man Vol. 2: Everyday Hero (Digest)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/20/4bc665483c3aa',
      extension: 'jpg'
    }
  },
  {
    id: 1003,
    title: 'Sentry, the (Trade Paperback)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/f/c0/4bc66d78f1bee',
      extension: 'jpg'
    }
  },
  {
    id: 323,
    title: 'Ant-Man (2003) #2',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/f/20/4bc69f33cafc0',
      extension: 'jpg'
    }
  },
  {
    id: 291,
    title: 'Ant-Man (2003) #1',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/e0/4bc6a2497684e',
      extension: 'jpg'
    }
  },
  {
    id: 1158,
    title: 'ULTIMATE X-MEN VOL. 5: ULTIMATE WAR TPB (Trade Paperback)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/2/f0/4bc6670c80007',
      extension: 'jpg'
    }
  },
  {
    id: 5813,
    title: 'Marvel Milestones (2005) #22',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
      extension: 'jpg'
    }
  },
  {
    id: 331,
    title: 'Gun Theory (2003) #4',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/60/4bc69f11baf75',
      extension: 'jpg'
    }
  },
  {
    id: 1220,
    title: 'Amazing Spider-Man 500 Covers Slipcase - Book II (Trade Paperback)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
      extension: 'jpg'
    }
  },
  {
    id: 376,
    title: 'Ant-Man (2003) #3',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/d/70/4bc69c7e9b9d7',
      extension: 'jpg'
    }
  },
  {
    id: 183,
    title: 'Startling Stories: The Incorrigible Hulk (2004) #1',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
      extension: 'jpg'
    }
  },
  {
    id: 2088,
    title:
      'Official Handbook of the Marvel Universe (2004) #14 (FANTASTIC FOUR)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/90/4bc6353e5fc56',
      extension: 'jpg'
    }
  },
  {
    id: 1749,
    title:
      'Official Handbook of the Marvel Universe (2004) #11 (X-MEN - AGE OF APOCALYPSE)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/b0/4bc6494ed6eb4',
      extension: 'jpg'
    }
  }
];

export const multipleComicsByIdAndName = [
  {
    id: 82967,
    title: 'Marvel Previews (2017)'
  },
  {
    id: 82965,
    title: 'Marvel Previews (2017)'
  },
  {
    id: 82970,
    title: 'Marvel Previews (2017)'
  },
  {
    id: 38041,
    title: 'X-Men (2010)'
  },
  {
    id: 23561,
    title: 'Holiday Special (1969) #1'
  },
  {
    id: 1886,
    title: 'Official Handbook of the Marvel Universe (2004) #12 (SPIDER-MAN)'
  },
  {
    id: 25582,
    title: 'Kabuki Reflections Vol. 1 (Hardcover)'
  },
  {
    id: 37376,
    title: 'Punishermax: Kingpin (2010)'
  },
  {
    id: 1158,
    title: 'ULTIMATE X-MEN VOL. 5: ULTIMATE WAR TPB (Trade Paperback)'
  },
  {
    id: 1994,
    title: 'Official Handbook of the Marvel Universe (2004) #13 (TEAMS)'
  },
  {
    id: 59543,
    title: 'Invincible Iron Man (2015) #11 (Keown Mighty Men Variant)'
  },
  {
    id: 5813,
    title: 'Marvel Milestones (2005) #22'
  },
  {
    id: 59539,
    title: 'Doctor Strange (2015) #10 (Henderson Mighty Men Variant)'
  },
  {
    id: 59555,
    title: 'Squadron Supreme (2015) #9 (Frison Mighty Men Variant)'
  },
  {
    id: 1308,
    title: 'Marvel Age Spider-Man Vol. 2: Everyday Hero (Digest)'
  },
  {
    id: 37534,
    title: 'Magician: Apprentice Riftwar Saga (2010) #17'
  },
  {
    id: 59927,
    title:
      'Darth Vader (2015) #25 (Christopher Action Figure Black and White Variant)'
  },
  {
    id: 1590,
    title:
      'Official Handbook of the Marvel Universe (2004) #9 (THE WOMEN OF MARVEL)'
  },
  {
    id: 59527,
    title: 'Black Panther (2016) #4 (Anacleto Mighty Men Variant)'
  },
  {
    id: 37529,
    title: 'Magician: Apprentice Riftwar Saga (2010) #13'
  },
  {
    id: 3627,
    title: 'Storm (2006)'
  },
  {
    id: 25856,
    title: 'MARVEL MASTERWORKS: THE UNCANNY X-MEN VOL. 3 HC (Trade Paperback)'
  },
  {
    id: 58584,
    title:
      'The Amazing Spider-Man (2015) #16 (Jimenez Black Panther 50th Anniversary Variant)'
  },
  {
    id: 59739,
    title: 'Civil War II: Kingpin (2016) #1 (Noto Character Variant)'
  },
  {
    id: 21171,
    title: 'Amazing Spider-Man (1999) #558 (Turner Variant)'
  },
  {
    id: 38002,
    title: 'X-Men: Fall Of The Mutants (2010)'
  },
  {
    id: 37502,
    title: 'Marvels Vol. 1 (1994) #5'
  },
  {
    id: 22582,
    title: 'Civil War (Hardcover)'
  },
  {
    id: 56802,
    title: 'Captain Britain: Legacy of A Legend (Trade Paperback)'
  },
  {
    id: 59558,
    title: 'The Totally Awesome Hulk (2015) #8 (Putri Mighty Men Variant)'
  },
  {
    id: 25321,
    title: 'Halo Chronicles (2009) #2'
  },
  {
    id: 59533,
    title: 'Captain America: Steve Rogers (2016) #3 (Hans Mighty Men Variant)'
  },
  {
    id: 17486,
    title: 'X-Men: Phoenix - Warsong (2006)'
  },
  {
    id: 37504,
    title: 'Marvels Vol. 1 (1994) #7'
  },
  {
    id: 37508,
    title: 'Official Marvel Universe Handbook (2009) #2'
  },
  {
    id: 58587,
    title: 'The Amazing Spider-Man (2015) #21 (Rivera Variant)'
  },
  {
    id: 1689,
    title:
      'Official Handbook of the Marvel Universe (2004) #10 (MARVEL KNIGHTS)'
  },
  {
    id: 2088,
    title:
      'Official Handbook of the Marvel Universe (2004) #14 (FANTASTIC FOUR)'
  },
  {
    id: 37497,
    title: 'Marvels Vol. 1 (1994) #1'
  },
  {
    id: 37501,
    title: 'Marvels Vol. 1 (1994) #6'
  },
  {
    id: 15878,
    title: 'Hedge Knight II: Sworn Sword (2007) #1 (Yu Variant)'
  },
  {
    id: 38008,
    title: 'Incredible Hulks: Dark Son (2010)'
  },
  {
    id: 58726,
    title: 'Haunted Mansion (2016) #5 (Young Variant)'
  },
  {
    id: 59551,
    title: 'Spider-Man (2016) #6 (Anka Mighty Men Variant)'
  },
  {
    id: 1220,
    title: 'Amazing Spider-Man 500 Covers Slipcase - Book II (Trade Paperback)'
  },
  {
    id: 59548,
    title: 'Old Man Logan (2016) #8 (Albuquerque Mighty Men Variant)'
  },
  {
    id: 27238,
    title: 'Wolverine Saga (2009) #7'
  },
  {
    id: 37503,
    title: 'Marvels Vol. 1 (1994) #8'
  },
  {
    id: 37302,
    title: 'Marvel Adventures Super Heroes Special (2010) #1'
  },
  {
    id: 37505,
    title: 'Marvels Vol. 1 (1994) #9'
  },
  {
    id: 61137,
    title: 'The Punisher (2016) #5 (Cosplay Variant)'
  },
  {
    id: 37507,
    title: 'Official Marvel Universe Handbook (2009) #1'
  },
  {
    id: 25320,
    title: 'Halo Chronicles (2009) #1'
  },
  {
    id: 1749,
    title:
      'Official Handbook of the Marvel Universe (2004) #11 (X-MEN - AGE OF APOCALYPSE)'
  },
  {
    id: 59560,
    title: 'Uncanny Inhumans (2015) #12 (Land Mighty Men Variant)'
  },
  {
    id: 20956,
    title: 'Penance: Relentless (2008)'
  },
  {
    id: 1332,
    title: 'X-Men: Days of Future Past (Trade Paperback)'
  },
  {
    id: 37531,
    title: 'Magician: Apprentice Riftwar Saga (2010) #15'
  },
  {
    id: 59559,
    title: 'Uncanny Avengers (2015) #11 (Hetrick Mighty Men Variant)'
  },
  {
    id: 58586,
    title:
      'The Amazing Spider-Man (2015) #19 (Veregge Black Panther 50th Anniversary Variant)'
  },
  {
    id: 37530,
    title: 'Magician: Apprentice Riftwar Saga (2010) #14'
  },
  {
    id: 26620,
    title: 'The Stand: American Nightmares HC (Hardcover)'
  },
  {
    id: 37500,
    title: 'Marvels Vol. 1 (1994) #4'
  },
  {
    id: 6181,
    title: 'Ultimate Spider-Man Ultimate Collection Book 1 (Trade Paperback)'
  },
  {
    id: 1003,
    title: 'Sentry, the (Trade Paperback)'
  },
  {
    id: 59181,
    title: 'Civil War II (2016) #6 (Gi Connecting Variant H)'
  },
  {
    id: 59557,
    title: 'The Astonishing Ant-Man (2015) #10 (Bagley Mighty Men Variant)'
  },
  {
    id: 15094,
    title: 'Silver Surfer (1987)'
  },
  {
    id: 27649,
    title: 'Incredible Hulks (2010) #604 (DJURDJEVIC 70TH ANNIVERSARY VARIANT)'
  },
  {
    id: 59754,
    title: 'Civil War II (2016) #6 (Sprouse Battle Variant)'
  },
  {
    id: 15808,
    title: 'Ultimate Spider-Man (2000) #110 (Mark Bagley Variant)'
  },
  {
    id: 291,
    title: 'Ant-Man (2003) #1'
  },
  {
    id: 60017,
    title: 'The Amazing Spider-Man (2015) #24 (Lozano Teaser Variant)'
  },
  {
    id: 22253,
    title: 'Hulk Custom Comic (2008) #1'
  },
  {
    id: 37499,
    title: 'Marvels Vol. 1 (1994) #3'
  },
  {
    id: 37533,
    title: 'Magician: Apprentice Riftwar Saga (2010) #16'
  },
  {
    id: 183,
    title: 'Startling Stories: The Incorrigible Hulk (2004) #1'
  },
  {
    id: 323,
    title: 'Ant-Man (2003) #2'
  },
  {
    id: 331,
    title: 'Gun Theory (2003) #4'
  },
  {
    id: 428,
    title: 'Ant-Man (2003) #4'
  },
  {
    id: 384,
    title: 'Gun Theory (2003) #3'
  },
  {
    id: 59525,
    title: 'The Amazing Spider-Man (2015) #15 (Panosian Mighty Men Variant)'
  },
  {
    id: 60015,
    title: 'The Amazing Spider-Man (2015) #22 (Lozano Teaser Variant)'
  },
  {
    id: 376,
    title: 'Ant-Man (2003) #3'
  },
  {
    id: 59524,
    title:
      'All-New, All-Different Avengers (2015) #12 (Kuder Mighty Men Variant)'
  },
  {
    id: 77793,
    title: 'Black Panther (2018) #23'
  },
  {
    id: 89510,
    title: 'The Amazing Spider-Man (2018) #55'
  },
  {
    id: 77946,
    title: 'Conan the Barbarian (2019) #18'
  },
  {
    id: 90341,
    title: 'S.W.O.R.D. (2020) #2'
  },
  {
    id: 91751,
    title: 'King in Black: Gwenom Vs. Carnage (2021) #1'
  },
  {
    id: 90006,
    title: 'MARVEL-VERSE: CAPTAIN MARVEL GN-TPB (Trade Paperback)'
  },
  {
    id: 81106,
    title: 'ETERNALS: THE DREAMING CELESTIAL SAGA TPB (Trade Paperback)'
  },
  {
    id: 82510,
    title: 'The Union (2020) #2'
  },
  {
    id: 90085,
    title: 'Marauders (2019) #17'
  },
  {
    id: 81374,
    title: 'BLACK CAT VOL. 3: ALL DRESSED UP TPB (Trade Paperback)'
  },
  {
    id: 90049,
    title: 'Captain America Facsimile Edition (2021) #354'
  },
  {
    id: 80541,
    title: 'Star Wars: Darth Vader (2020) #9'
  },
  {
    id: 81164,
    title: 'THOR AND THE ETERNALS: THE CELESTIALS SAGA TPB (Trade Paperback)'
  },
  {
    id: 89512,
    title: 'The Amazing Spider-Man (2018) #57'
  },
  {
    id: 89658,
    title: 'Immortal Hulk (2018) #42'
  }
];
