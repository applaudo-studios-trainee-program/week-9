export const singleCharacterDataMock = [
  {
    id: 1017100,
    name: 'A-Bomb (HAS)',
    description:
      "Rick Jones has been Hulk's best bud since day one, but now he's more than a friend...he's a teammate! Transformed by a Gamma energy explosion, A-Bomb's thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction! ",
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16',
      extension: 'jpg'
    },
    comics: {
      available: 3,
      collectionURI:
        'http://gateway.marvel.com/v1/public/characters/1017100/comics',
      items: [
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/40632',
          name: 'Hulk (2008) #53'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/40630',
          name: 'Hulk (2008) #54'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/40628',
          name: 'Hulk (2008) #55'
        }
      ],
      returned: 3
    },
    stories: {
      available: 7,
      collectionURI:
        'http://gateway.marvel.com/v1/public/characters/1017100/stories',
      items: [
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/92078',
          name: 'Hulk (2008) #55',
          type: 'cover'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/92079',
          name: 'Interior #92079',
          type: 'interiorStory'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/92082',
          name: 'Hulk (2008) #54',
          type: 'cover'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/92083',
          name: 'Interior #92083',
          type: 'interiorStory'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/92086',
          name: 'Hulk (2008) #53',
          type: 'cover'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/92087',
          name: 'Interior #92087',
          type: 'interiorStory'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/105929',
          name: 'cover from Free Comic Book Day 2013 (Avengers/Hulk) (2013) #1',
          type: 'cover'
        }
      ],
      returned: 7
    }
  }
];

export const multipleCharactersByFilterDataMock = [
  {
    id: 1011176,
    name: 'Ajak',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/2/80/4c002f35c5215',
      extension: 'jpg'
    }
  },
  {
    id: 1009149,
    name: 'Abyss',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/30/535feab462a64',
      extension: 'jpg'
    }
  },
  {
    id: 1009144,
    name: 'A.I.M.',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/20/52602f21f29ec',
      extension: 'jpg'
    }
  }
];

export const multipleCharactersDataMock = [
  {
    id: 1017100,
    name: 'A-Bomb (HAS)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16',
      extension: 'jpg'
    }
  },
  {
    id: 1009144,
    name: 'A.I.M.',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/20/52602f21f29ec',
      extension: 'jpg'
    }
  },
  {
    id: 1010699,
    name: 'Aaron Stack',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
      extension: 'jpg'
    }
  },
  {
    id: 1009146,
    name: 'Abomination (Emil Blonsky)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/50/4ce18691cbf04',
      extension: 'jpg'
    }
  },
  {
    id: 1016823,
    name: 'Abomination (Ultimate)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
      extension: 'jpg'
    }
  },
  {
    id: 1009148,
    name: 'Absorbing Man',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/1/b0/5269678709fb7',
      extension: 'jpg'
    }
  },
  {
    id: 1009149,
    name: 'Abyss',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/30/535feab462a64',
      extension: 'jpg'
    }
  },
  {
    id: 1010903,
    name: 'Abyss (Age of Apocalypse)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/80/4c00358ec7548',
      extension: 'jpg'
    }
  },
  {
    id: 1011266,
    name: 'Adam Destine',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
      extension: 'jpg'
    }
  },
  {
    id: 1010354,
    name: 'Adam Warlock',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/a/f0/5202887448860',
      extension: 'jpg'
    }
  },
  {
    id: 1010846,
    name: 'Aegis (Trey Rollins)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/5/e0/4c0035c9c425d',
      extension: 'gif'
    }
  },
  {
    id: 1011297,
    name: 'Agent Brand',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/4/60/52695285d6e7e',
      extension: 'jpg'
    }
  },
  {
    id: 1011031,
    name: 'Agent X (Nijo)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
      extension: 'jpg'
    }
  },
  {
    id: 1009150,
    name: 'Agent Zero',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/f/60/4c0042121d790',
      extension: 'jpg'
    }
  },
  {
    id: 1011198,
    name: 'Agents of Atlas',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/a0/4ce18a834b7f5',
      extension: 'jpg'
    }
  },
  {
    id: 1011175,
    name: 'Aginar',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
      extension: 'jpg'
    }
  },
  {
    id: 1011136,
    name: 'Air-Walker (Gabriel Lan)',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
      extension: 'jpg'
    }
  },
  {
    id: 1011176,
    name: 'Ajak',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/2/80/4c002f35c5215',
      extension: 'jpg'
    }
  },
  {
    id: 1010870,
    name: 'Ajaxis',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/70/4c0035adc7d3a',
      extension: 'jpg'
    }
  },
  {
    id: 1011194,
    name: 'Akemi',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
      extension: 'jpg'
    }
  }
];
