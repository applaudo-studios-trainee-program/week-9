export const singleStoryDataMock = [
  {
    id: 744,
    title: 'Uncanny X-Men (1963) #460',
    description:
      'The Uncanny X-Men just can’t get a break. As the team tries to relax and deal with the return of their once-dead teammate Psylocke, guess who crashes their party?',
    comics: {
      available: 1,
      collectionURI: 'http://gateway.marvel.com/v1/public/stories/744/comics',
      items: [
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/2073',
          name: 'Uncanny X-Men (1963) #460'
        }
      ],
      returned: 1
    }
  }
];

export const multipleStoriesDataMock = [
  {
    id: 7,
    title:
      'Investigating the murder of a teenage girl, Cage suddenly learns that a three-way gang war is under way for control of the turf',
    description: ''
  },
  {
    id: 8,
    title:
      'In the wake of September 11th, the world watched as firefighters, police officers and EMT workers selflessly risked their lives',
    description: ''
  },
  {
    id: 9,
    title:
      'Ordinary New York City cop Frankie &QUOT;Gunz&QUOT; Gunzer now has a new call to duty Ñ not just to uphold the law, but to save ',
    description: ''
  },
  {
    id: 10,
    title:
      'In this thought-provoking anthology, a world-class collection of top comic-book creators from around the globe presents a series',
    description: ''
  },
  {
    id: 11,
    title: 'Interior #11',
    description: ''
  },
  {
    id: 12,
    title:
      "Presenting visionary writer/artist Frank Miller's unique take on the world-famous wall-crawler Ñ including appearances by the Pu",
    description: ''
  },
  {
    id: 14,
    title:
      "Karen Page, Daredevil's former lover, trades away Daredevil's identity for a drug fix.  Matt Murdock must find strength as the K",
    description: ''
  },
  {
    id: 15,
    title:
      "This classic tale explores Matt Murdock's formative years Ñ detailing the relationship between him and his father, and the event",
    description: ''
  },
  {
    id: 16,
    title:
      "Frank Miller's Daredevil submerged us in a world where heroes, pushed to the brink of madness, were allowed to mirror the human ",
    description: ''
  },
  {
    id: 17,
    title:
      "This volume features the gritty, street-level action and moody atmosphere that made Miller's Daredevil classic Ñ including appea",
    description: ''
  },
  {
    id: 18,
    title:
      "Concluding Miller's complete run,  the Man Without Fear faces the Kingpin of Crime and the Hand Ñ and must come  to grips with t",
    description: ''
  },
  {
    id: 20,
    title:
      "Maya Lopez Ñ deaf, but capable of assimilating an individual's fighting style on sight Ñ has set her sights on Daredevil, who sh",
    description: ''
  },
  {
    id: 22,
    title:
      "A contract on Matt Murdock's life is somehow connected to the Kingpin's apparent murder at the hands of the mysterious Mr. Silke",
    description: ''
  },
  {
    id: 23,
    title:
      "A down-on-his-luck FBI agent has sold the hero's most guarded secret to a tabloid newspaper, exposing Daredevil's identity to th",
    description: ''
  },
  {
    id: 24,
    title:
      'Elektra: Assassin Ñ the story of how a tragic murder transformed an innocent, na•ve college student into a deadly assassin, and ',
    description: ''
  },
  {
    id: 26,
    title:
      "Haunted by her own father's death, Elektra finds that killing her latest target's daughter Ñ the only witness to the murder Ñ is",
    description: ''
  },
  {
    id: 27,
    title:
      'Despite the fact that Elektra died in his arms, Daredevil is plagued by recurring nightmares and terrible premonitions that his ',
    description: ''
  },
  {
    id: 28,
    title:
      "S.H.I.E.L.D. offers Elektra a mission so dirty, so difficult, no one else could pull it off.  But if Elektra takes the job, she'",
    description: ''
  },
  {
    id: 29,
    title:
      'Caught in the heart of a nuclear explosion, mild-mannered scientist Bruce Banner finds himself Ñ during moments of extreme stres',
    description: ''
  },
  {
    id: 30,
    title:
      "The second volume containing the Hulk's early adventures with appearances by the Sub-Mariner, the Mandarin, Ka-Zar and Nick Fury",
    description: ''
  }
];

export const multipleStoriesByIdAndName = [
  {
    id: 7,
    title:
      'Investigating the murder of a teenage girl, Cage suddenly learns that a three-way gang war is under way for control of the turf'
  },
  {
    id: 8,
    title:
      'In the wake of September 11th, the world watched as firefighters, police officers and EMT workers selflessly risked their lives'
  },
  {
    id: 9,
    title:
      'Ordinary New York City cop Frankie &QUOT;Gunz&QUOT; Gunzer now has a new call to duty Ñ not just to uphold the law, but to save '
  },
  {
    id: 10,
    title:
      'In this thought-provoking anthology, a world-class collection of top comic-book creators from around the globe presents a series'
  },
  {
    id: 11,
    title: 'Interior #11'
  },
  {
    id: 12,
    title:
      "Presenting visionary writer/artist Frank Miller's unique take on the world-famous wall-crawler Ñ including appearances by the Pu"
  },
  {
    id: 14,
    title:
      "Karen Page, Daredevil's former lover, trades away Daredevil's identity for a drug fix.  Matt Murdock must find strength as the K"
  },
  {
    id: 15,
    title:
      "This classic tale explores Matt Murdock's formative years Ñ detailing the relationship between him and his father, and the event"
  },
  {
    id: 16,
    title:
      "Frank Miller's Daredevil submerged us in a world where heroes, pushed to the brink of madness, were allowed to mirror the human "
  },
  {
    id: 17,
    title:
      "This volume features the gritty, street-level action and moody atmosphere that made Miller's Daredevil classic Ñ including appea"
  },
  {
    id: 18,
    title:
      "Concluding Miller's complete run,  the Man Without Fear faces the Kingpin of Crime and the Hand Ñ and must come  to grips with t"
  },
  {
    id: 20,
    title:
      "Maya Lopez Ñ deaf, but capable of assimilating an individual's fighting style on sight Ñ has set her sights on Daredevil, who sh"
  },
  {
    id: 22,
    title:
      "A contract on Matt Murdock's life is somehow connected to the Kingpin's apparent murder at the hands of the mysterious Mr. Silke"
  },
  {
    id: 23,
    title:
      "A down-on-his-luck FBI agent has sold the hero's most guarded secret to a tabloid newspaper, exposing Daredevil's identity to th"
  },
  {
    id: 24,
    title:
      'Elektra: Assassin Ñ the story of how a tragic murder transformed an innocent, na•ve college student into a deadly assassin, and '
  },
  {
    id: 26,
    title:
      "Haunted by her own father's death, Elektra finds that killing her latest target's daughter Ñ the only witness to the murder Ñ is"
  },
  {
    id: 27,
    title:
      'Despite the fact that Elektra died in his arms, Daredevil is plagued by recurring nightmares and terrible premonitions that his '
  },
  {
    id: 28,
    title:
      "S.H.I.E.L.D. offers Elektra a mission so dirty, so difficult, no one else could pull it off.  But if Elektra takes the job, she'"
  },
  {
    id: 29,
    title:
      'Caught in the heart of a nuclear explosion, mild-mannered scientist Bruce Banner finds himself Ñ during moments of extreme stres'
  },
  {
    id: 30,
    title:
      "The second volume containing the Hulk's early adventures with appearances by the Sub-Mariner, the Mandarin, Ka-Zar and Nick Fury"
  },
  {
    id: 31,
    title:
      'Featuring the birth of an unlikely hero and the early adventures that paved the way for the mega-popular media icon that Spider-'
  },
  {
    id: 32,
    title:
      "Spider-Man's earliest adventures continue with classic stories featuring the Vulture, Dr. Octopus, Mysterio and the Shocker."
  },
  {
    id: 33,
    title:
      'In this volume, Peter faces some of his greatest challenges and his most harrowing foes such as Doc Ock and the Kingpin. Featuri'
  },
  {
    id: 34,
    title:
      "Spider-Man's earliest adventures continue with tales to surprise and delight. Appearances by Iceman, Morbius, the Prowler, Green"
  },
  {
    id: 35,
    title:
      'Telepathic teacher Charles Xavier assembles his first recruits Ñ Cyclops, Angel, Iceman, Beast and Marvel Girl Ñ and trains them'
  },
  {
    id: 36,
    title:
      "Experience Wolverine's battle to keep the feral berserker within in check, while trying to be the best there is at what he does."
  },
  {
    id: 37,
    title:
      'Forever locked in struggle to keep his berserker fury in check, Logan is a man without a past on a never-ending journey to uncov'
  },
  {
    id: 38,
    title:
      'This volume continues the stories of the enigmatic Wolverine Ñ gifted with heightened senses, a healing factor and razor-sharp c'
  },
  {
    id: 39,
    title:
      'Relive the second generation of X-Men, fighting villains like the Juggernaut and Magneto, and discover the human within the hero'
  },
  {
    id: 40,
    title:
      'Ever-expanding their ranks, the Children of the Atom combat the evils threatening both mutants and humans Ñ like the Brotherhood'
  },
  {
    id: 41,
    title:
      'Hated and feared by a world they are sworn to protect, the X-Men face off against some of their most unforgettable foes Ñ includ'
  },
  {
    id: 42,
    title:
      'Wolverine, Storm, Rogue, Colossus, Nightcrawler and more of the world-famous X-Men band together to protect a world that hates a'
  },
  {
    id: 43,
    title:
      'Presenting the first adventures of Spider-Man, Wolverine, Daredevil, the Hulk, the Fantastic Four and other Marvel icons.'
  },
  {
    id: 44,
    title:
      "One-eyed war-horse Nick Fury isn't quite ready to retire  Ñ and when it all hits the fan, S.H.I.E.L.D. realizes that there's onl"
  },
  {
    id: 45,
    title:
      "This deluxe, hardcover volume collects the first year's stories from Incredible Hulk Vol. 1: Return of the Monster, Incredible H"
  },
  {
    id: 49,
    title:
      "This fully illustrated, comprehensive hardcover includes biographies, statistics, essential reading on all Mighty Marvel's most "
  },
  {
    id: 51,
    title:
      'To combat a plant-devouring virus, and a monarch who has power to back up dreams of domination, these new heroes and some old fr'
  },
  {
    id: 54,
    title:
      'This deluxe hardcover volume collects the New X-Men material found in New X-Men Vol. 1: E is for Extinction and New X-Men Vol. 2'
  },
  {
    id: 55,
    title:
      'A new foe, voted favorite villain by the Wizard Fan Awards, uncovers a city populated by a deadly technology and unleashes mass ',
    description: ''
  },
  {
    id: 56,
    title:
      ' As protesters lay siege to the Xavier Institute, Professor X lies in a coma, trapped within the shattered form of his evil twin'
  },
  {
    id: 57,
    title:
      'From the teeming city streets of Mutant Town to the irradiated ruins of the island nation Genosha, once a haven for mutants, the'
  },
  {
    id: 58,
    title:
      "Nick Fury finds himself at odds with the covert company he's run for many years Ñ or thought he's run."
  },
  {
    id: 59,
    title:
      'Suave secret agent Nick Fury faces the mysterious enemy operative known only as Scorpio with the fate of the free world at stake'
  },
  {
    id: 63,
    title:
      'Two unlucky cops draw the unenviable task of capturing the Punisher, while the ruthless Ma Gnucci and her gang will stop at noth'
  },
  {
    id: 64,
    title:
      'Following a team-up with Spider-Man, the Punisher heads to the remote scrap of dirt known as Grand Nixon Island to take down an '
  },
  {
    id: 66,
    title:
      'On the edge of alcoholism and a failed marriage, Bob Reynolds wakes up to discover his true nature. A forgotten hero, he must un'
  },
  {
    id: 67,
    title:
      'After a wildly popular run on Spider-Man, Todd McFarlane held the single-comic sales record. He then went on to create the multi'
  },
  {
    id: 71,
    title:
      "In this shocking and moving story, the Green Goblin and Spider-Man face off atop the George Washington Bridge, with Gwen's life "
  },
  {
    id: 72,
    title:
      'Tormented by his unstoppable alter ego, Bruce Banner has nowhere to hide. Having located the man-like monstrosity that is the Hu'
  },
  {
    id: 73,
    title:
      'Fired by Tony Stark and recruited by S.H.I.E.L.D., James Rhodes must lead a team of rookies against the evil alliance known as A'
  },
  {
    id: 76,
    title:
      'This deluxe hardcover reprints the material from Ultimate Spider-Man Vol. 1: Power and Responsibility and Ultimate Spider-Man Vo'
  },
  {
    id: 80,
    title:
      'The X-Men use their genetic gifts to protect a world that fears and hates them from other mutants who would use their powers for'
  },
  {
    id: 84,
    title:
      'The most popular member of the X-Men goes solo when Wolverine travels to Japan, testing his honor and risking his life for the o'
  },
  {
    id: 85,
    title:
      "When Wolverine rebuffs the reconstituted Weapon X program, management is forced to settle for second best Ñ but Deadpool's first"
  },
  {
    id: 86,
    title:
      "There's a new killer in town, and he wants Wolverine's title all to himself. Who is the mysterious Mr. X, and how far is he will"
  },
  {
    id: 87,
    title:
      "Adored by humans, reviled by their fellow mutants, X-Force does the dirty jobs that others can't, or won't. All they want in ret"
  },
  {
    id: 88,
    title:
      'Break out the Kleenex Ñ and the body bags Ñ as the X-Force embarks on its most hazardous mission yet.  Whose swan song will it b'
  },
  {
    id: 89,
    title:
      'Long persecuted for their genetic differences, some mutants are cashing in on their special gifts. Widely accepted by the genera'
  },
  {
    id: 90,
    title:
      'The world is once again threatened by the mutant terrorist Magneto and only the X-Men united can stop him. And should they survi'
  },
  {
    id: 91,
    title:
      'One of the X-Men, Jean Grey, has unwittingly attained power beyond imagination.  As the Dark Phoenix, she is capable of incinera'
  },
  {
    id: 93,
    title:
      'The vicious Marauders have been employed to wipe out the underground mutant community known as the Morlocks, whose only hope of '
  },
  {
    id: 96,
    title:
      'The Avengers and Defenders find themselves the pawns in an intergalactic game of chess when the trickster god Loki enlists the a'
  },
  {
    id: 97,
    title:
      'The Celestial Madonna, who will conceive a being of immense cosmic power, awakens and has the Avengers facing off against three '
  },
  {
    id: 98,
    title:
      "The Grim Reaper comes calling on New York City's Avengers Day, and Earth's mightiest heroes face off against munitions despot Mo"
  },
  {
    id: 99,
    title:
      'When duty calls, the legendary Avengers answer Ñ but Kang is a foe unlike any other; his ultimate goal is nothing short of globa'
  },
  {
    id: 102,
    title:
      'The lives of a handful of gunfighters gave birth to the West of legend, and their legends became more real than their lives. Thi'
  },
  {
    id: 105,
    title:
      'Captain America had disappeared and was assumed dead.  After an unexpected return, he must rediscover himself and the world he l'
  },
  {
    id: 106,
    title:
      'He is the son of Mar-Vell, once the greatest warrior the galaxy had ever known. Spawned in a test tube and prematurely aged to m'
  },
  {
    id: 107,
    title:
      'This collection gathers the team-ups and battles between Spider-Man and Superman, the Hulk and Batman, and the X-Men and the New'
  },
  {
    id: 108,
    title:
      'Featuring team-ups and battles between Hulk and Superman, Generation X and Gen 13, Team X and Team 7, Daredevil and Batman, and '
  },
  {
    id: 109,
    title:
      'Rookie reporter Katherine Farrell is stuck with the super-hero beat unless she can nail a big story and move up to the City Crim'
  },
  {
    id: 111,
    title:
      'Armed with the Cloak of Levitation and the all-seeing Eye of Agamotto, Dr. Strange defends the planet from mystical menaces and '
  },
  {
    id: 112,
    title:
      'Earth X explores the depths and heights of the Marvel Universe from the roots of its humble beginnings to the peak of its ultima'
  },
  {
    id: 113,
    title:
      'The critically acclaimed trilogy begins its next installment as we find things in this alternate future timeline not working out'
  },
  {
    id: 114,
    title:
      'Much is revealed in the final chapter of Universe X: Why has Mephisto been manipulating Thanos, Nighthawk, Gargoyle and Dr. Doom'
  },
  {
    id: 115,
    title:
      'Re-presenting the formation of the Avengers, the revival of Captain America, enemies such as the Sub-Mariner, and Kang the Conqu'
  },
  {
    id: 118,
    title:
      'Witness the first daring attempts of the villainous Cobra Commander and his minions to cause havoc around the world, only to hav'
  },
  {
    id: 119,
    title:
      'Cobra launches an unprecedented strike on Washington, D.C. and the Joes engage the enemy in an action-packed display of strategy'
  },
  {
    id: 120,
    title:
      "The original adventures of America's elite special-missions force continue with the recruitment of Roadblock and Duke; the emerg"
  },
  {
    id: 121,
    title:
      "The first appearances of some of the most popular characters in the title's long run Ñ including G.I. Joe team members Flint, La"
  },
  {
    id: 122,
    title:
      'Cobra advances its cruel campaign for world domination with the creation of a new country where the ruthless terrorist organizat'
  },
  {
    id: 124,
    title:
      'Howard finds himself bizarrely transformed into a succession of other creatures, and he and Bev come up against an ancient mysti'
  },
  {
    id: 125,
    title:
      'Spider-Man. Dr. Strange. Adam Warlock. Pip the Troll. Captain Marvel. Five classic costumed champions, whose combined strength c'
  },
  {
    id: 126,
    title:
      "The mad Titan, Thanos, seeks to reshape the universe in the visage of his true love, Death. Only the combined forces of Earth's "
  },
  {
    id: 127,
    title:
      'Captain Marvel wielded his cosmic powers in defense of the galaxy, but exposure to a carcinogenic nerve gas causes him to succum'
  },
  {
    id: 130,
    title:
      'Kraven the Hunter has stalked and killed every animal known to man. But there is one beast that has eluded him. One quarry that '
  },
  {
    id: 131,
    title:
      "When a corrupt corporation blocks Dr. Curt Conners' bid to help cure his cancer-stricken wife, his only hope for curing Martha m"
  },
  {
    id: 133,
    title:
      'Few people have ever left their mark on one character quite the way Walter Simonson has. His work on the Mighty Thor swept the N'
  },
  {
    id: 134,
    title:
      'An ancient enemy walks the world of Midgard, know to us as Earth. Victory comes at a terrible price Ñ one that will alter foreve'
  },
  {
    id: 135,
    title:
      "After Odin's death, Thor becomes lord and master of the Asgard Ñ leaving Earth in the hands of the untested Thor-Girl. An old fo"
  },
  {
    id: 136,
    title:
      "It doesn't take long for Spider-Man to encounter the other heroes in the Ultimate Marvel Universe along with the devious villain"
  },
  {
    id: 137,
    title:
      'A wild evil mutant comes of age, celebrating with superhuman mayhem. And in London, the X-Men must stop Mr. Clean, a self-styled'
  },
  {
    id: 138,
    title:
      'Armed with a silver dagger and Webley .455-caliber pistol, Joey Chapman is the latest countryman to take up the mantle of Union '
  },
  {
    id: 139,
    title:
      'Of all the members of the X-Men, none has been more popular than Wolverine -- yet the origins of this mutant hero had always bee'
  },
  {
    id: 140,
    title:
      "The X-Men follow the trail of Rogue's nightmares to a hidden corner of the globe where the great dinosaurs of the Jurassic and C"
  },
  {
    id: 141,
    title:
      'Together with Ka-Zar and Zabu, the X-Men and Spider-Man must struggle to stave off disaster and discover the terrible world-thre'
  },
  {
    id: 142,
    title:
      'The X-Men charge into oblivion to protect a world that hates and fears them, Angel undergoes a dark transformation, and the youn'
  },
  {
    id: 146,
    title:
      'In the aftermath of 9/11, Cap must come to grips with a changing global landscape. From the ruins of the World Trade Center to t'
  },
  {
    id: 147,
    title:
      'Volume III collects two storylines: &QUOT;Trial of the Century&QUOT; and &QUOT;Lowlife.&QUOT;   In &QUOT;Trial of the Century,&Q'
  },
  {
    id: 149,
    title:
      "This time Frank's in South America, where he must rescue a Mafia don from angry guerilla fighters Ñ but if mercenaries don't kil"
  }
];
