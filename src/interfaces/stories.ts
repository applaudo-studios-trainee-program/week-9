export interface Item {
  resourceURI: string;
  name: string;
  role?: string;
}

export interface Characters {
  available: number;
  collectionURI: string;
  items: Item[];
  returned: number;
}

export interface OriginalIssue {
  resourceURI: string;
  name: string;
}

export interface Result {
  id: number;
  title: string;
  description: string;
  resourceURI: string;
  type: string;
  modified: string;
  thumbnail: null;
  creators: Characters;
  characters: Characters;
  series: Characters;
  comics: Characters;
  events: Characters;
  originalIssue: OriginalIssue;
}

export interface Data {
  offset: number;
  limit: number;
  total: number;
  count: number;
  results: Result[];
}

export interface StoriesResponse {
  code: number;
  status: string;
  copyright: string;
  attributionText: string;
  attributionHTML: string;
  etag: string;
  data: Data;
}

export interface Stories {
  id: number;
  title: string;
}

export interface Story {
  id: number;
  title: string;
  comicItems: Item[];
  description: string;
}
