import { Comics } from 'interfaces/comics';
import { Filters } from 'interfaces/common';

export type ComicsFilters = Filters<'format' | 'titleStartsWith'>;

export type ComicsState = {
  data: Comics[];
  filterData: ComicsFilters | null;
  offset: number;
  removedElements: number[];
  total: number;
};

export type ComicsActions =
  | {
      type: 'SET_DATA';
      payload: Pick<ComicsState, 'total' | 'data' | 'offset'>;
    }
  | {
      type: 'SET_FILTER_DATA';
      payload: Pick<ComicsState, 'filterData'>;
    }
  | {
      type: 'REMOVE_FILTER';
    }
  | { type: 'ADD_ENTRY_TO_HIDE'; payload: { comicId: number } };

export const comicsReducer = (
  state: ComicsState,
  action: ComicsActions
): ComicsState => {
  switch (action.type) {
    case 'SET_DATA': {
      const { removedElements } = state;
      const { total, data, offset } = action.payload;

      const filteredData = removedElements.length
        ? data.filter(comic => !removedElements.includes(comic.id))
        : data;

      return { ...state, data: filteredData, total, offset };
    }

    case 'SET_FILTER_DATA': {
      const { filterData } = action.payload;

      return {
        ...state,
        filterData: { ...state.filterData, ...filterData }
      };
    }

    case 'REMOVE_FILTER': {
      return { ...state, filterData: null };
    }

    case 'ADD_ENTRY_TO_HIDE': {
      const { comicId } = action.payload;

      return {
        ...state,
        removedElements: [...state.removedElements, comicId]
      };
    }

    default:
      return state;
  }
};
