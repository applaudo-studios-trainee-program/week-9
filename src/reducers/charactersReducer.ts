import { Characters } from 'interfaces/characters';
import { Filters } from 'interfaces/common';

export type CharactersFilters = Filters<
  'nameStartsWith' | 'comics' | 'stories'
>;

export type CharactersState = {
  data: Characters[];
  filterData: CharactersFilters | null;
  offset: number;
  removedElements: number[];
  total: number;
};

export type CharactersActions =
  | {
      type: 'SET_DATA';
      payload: Pick<CharactersState, 'total' | 'data' | 'offset'>;
    }
  | {
      type: 'SET_FILTER_DATA';
      payload: Pick<CharactersState, 'filterData'>;
    }
  | {
      type: 'REMOVE_FILTER';
    }
  | { type: 'ADD_ENTRY_TO_HIDE'; payload: { characterId: number } };

export const charactersReducer = (
  state: CharactersState,
  action: CharactersActions
): CharactersState => {
  switch (action.type) {
    case 'SET_DATA': {
      const { removedElements } = state;
      const { total, data, offset } = action.payload;

      const filteredData = removedElements.length
        ? data.filter(character => !removedElements.includes(character.id))
        : data;

      return { ...state, data: filteredData, total, offset };
    }

    case 'SET_FILTER_DATA': {
      const { filterData } = action.payload;

      return {
        ...state,
        filterData: { ...state.filterData, ...filterData }
      };
    }

    case 'REMOVE_FILTER': {
      return { ...state, filterData: null };
    }

    case 'ADD_ENTRY_TO_HIDE': {
      const { characterId } = action.payload;

      return {
        ...state,
        removedElements: [...state.removedElements, characterId]
      };
    }

    default:
      return state;
  }
};
