export type BookmarksTypes =
  | 'comicBookmarks'
  | 'storiesBookmarks'
  | 'characterBookmarks';

export type BookmarksState = {
  characterBookmarks: number[];
  comicBookmarks: number[];
  storiesBookmarks: number[];
};

export type BookmarksActions =
  | {
      type: 'ADD_BOOKMARK';
      payload: {
        bookmarkListName: BookmarksTypes;
        bookmarkId: number;
      };
    }
  | { type: 'REMOVE_BOOKMARK'; payload: { bookmarkId: number } }
  | { type: 'REMOVE_ALL_BOOKMARKS' };

export const bookmarksReducer = (
  state: BookmarksState,
  action: BookmarksActions
): BookmarksState => {
  switch (action.type) {
    case 'ADD_BOOKMARK': {
      const { bookmarkListName, bookmarkId } = action.payload;

      return {
        ...state,
        [bookmarkListName]: [...state[bookmarkListName], bookmarkId]
      };
    }

    case 'REMOVE_BOOKMARK': {
      const { bookmarkId } = action.payload;

      return {
        characterBookmarks: state.characterBookmarks.filter(
          characterId => characterId !== bookmarkId
        ),

        comicBookmarks: state.comicBookmarks.filter(
          comicId => comicId !== bookmarkId
        ),

        storiesBookmarks: state.storiesBookmarks.filter(
          storyId => storyId !== bookmarkId
        )
      };
    }

    case 'REMOVE_ALL_BOOKMARKS': {
      return {
        characterBookmarks: [],
        comicBookmarks: [],
        storiesBookmarks: []
      };
    }

    default:
      return state;
  }
};
