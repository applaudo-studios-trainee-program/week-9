import { Filters } from 'interfaces/common';
import { Stories } from 'interfaces/stories';

export type StoriesFilters = Filters<'modifiedSince'>;

export type StoriesState = {
  data: Stories[];
  filterData: StoriesFilters | null;
  offset: number;
  removedElements: number[];
  total: number;
};

export type StoriesActions =
  | {
      type: 'SET_DATA';
      payload: Pick<StoriesState, 'total' | 'data' | 'offset'>;
    }
  | {
      type: 'SET_FILTER_DATA';
      payload: Pick<StoriesState, 'filterData'>;
    }
  | {
      type: 'REMOVE_FILTER';
    }
  | { type: 'ADD_ENTRY_TO_HIDE'; payload: { storyId: number } };

export const storiesReducer = (
  state: StoriesState,
  action: StoriesActions
): StoriesState => {
  switch (action.type) {
    case 'SET_DATA': {
      const { removedElements } = state;
      const { total, data, offset } = action.payload;

      const filteredData = removedElements.length
        ? data.filter(story => !removedElements.includes(story.id))
        : data;

      return { ...state, data: filteredData, total, offset };
    }

    case 'SET_FILTER_DATA': {
      const { filterData } = action.payload;

      return {
        ...state,
        filterData: { ...state.filterData, ...filterData }
      };
    }

    case 'REMOVE_FILTER': {
      return { ...state, filterData: null };
    }

    case 'ADD_ENTRY_TO_HIDE': {
      const { storyId } = action.payload;

      return {
        ...state,
        removedElements: [...state.removedElements, storyId]
      };
    }

    default:
      return state;
  }
};
